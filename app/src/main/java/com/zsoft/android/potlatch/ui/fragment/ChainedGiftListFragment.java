package com.zsoft.android.potlatch.ui.fragment;

import android.accounts.Account;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zsoft.android.potlatch.AppPreferences;
import com.zsoft.android.potlatch.Constants;
import com.zsoft.android.potlatch.R;
import com.zsoft.android.potlatch.model.Gift;
import com.zsoft.android.potlatch.rest.api.GiftOperations;
import com.zsoft.android.potlatch.ui.adapter.AbstractGiftListAdapter;
import com.zsoft.android.potlatch.ui.adapter.GiftListAdapter;
import com.zsoft.android.potlatch.util.AccountUtils;
import com.zsoft.android.potlatch.util.GiftOrder;

import java.text.MessageFormat;
import java.util.ArrayList;

public class ChainedGiftListFragment extends PaginatedGiftListFragment implements View.OnClickListener {
    protected static final String TAG = ChainedGiftListFragment.class.getSimpleName();

    private Gift mParentGift;

    public ChainedGiftListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View top = super.onCreateView(inflater, container, savedInstanceState);

        Bundle bundle = getActivity().getIntent().getExtras();
        mParentGift = (Gift) bundle.getSerializable(Constants.EXTRA_GIFT);

        if (mParentGift != null) {
            String title = mParentGift.getTitle();
            if (!TextUtils.isEmpty(title)) {
                getActivity().setTitle(MessageFormat.format(
                        getActivity().getString(R.string.gift_chain_title_template), title));
            }
        }

        return top;
    }

    @Override
    protected void configureGiftCreateIntent(final Intent intent) {
        intent.putExtra(Constants.EXTRA_GIFT, mParentGift);
    }

    protected AbstractGiftListAdapter createAdapter() {
        Account chosenAccount = AccountUtils.getActiveAccount(getActivity());
        Long userId = null;
        if (chosenAccount != null) {
            userId = AccountUtils.getProfileId(getActivity());
        }
        return new GiftListAdapter(getActivity(), this, userId);
    }

    @Override
    public void onClick(View v) {
        Object tag = v.getTag();
        if (tag != null) {
            final int position = Integer.parseInt(tag.toString());
            Gift gift = getAdapter().getItem(position);
            switch (v.getId()) {
                case R.id.gift_delete:
                    deleteGift(gift.getId());
                    break;
            }
        }
    }

    @Override
    protected ArrayList<Gift> loadPage(String query, int pageId, int pageSize, int order) {
        if (mParentGift == null || mParentGift.getId() == null) {
            return null;
        }

        boolean includeNegativeRated = !AppPreferences.hideObsceneGifts(getActivity());

        GiftOrder ordering = GiftOrder.getOrder(order);
        GiftOperations giftOperations = mPotlatch.giftOperations();
        return new ArrayList<Gift>(giftOperations.searchChainedGifts(
                mParentGift.getId(), query, includeNegativeRated, pageId, pageSize,
                ordering.getOrderBy(), ordering.isAscendingOrder()));
    }

    protected ArrayList<Gift> loadPage(int pageId, int pageSize, int order) {
        if (mParentGift == null || mParentGift.getId() == null) {
            return null;
        }

        boolean includeNegativeRated = !AppPreferences.hideObsceneGifts(getActivity());

        GiftOrder ordering = GiftOrder.getOrder(order);
        GiftOperations giftOperations = mPotlatch.giftOperations();
        return new ArrayList<Gift>(giftOperations.getChainedGifts(
                mParentGift.getId(), includeNegativeRated, pageId, pageSize,
                ordering.getOrderBy(), ordering.isAscendingOrder()));
    }
}