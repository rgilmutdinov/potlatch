package com.zsoft.android.potlatch.ui.activity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.zsoft.android.potlatch.util.GiftOrder;
import com.zsoft.android.potlatch.R;
import com.zsoft.android.potlatch.ui.fragment.MyGiftListFragment;
import com.zsoft.android.potlatch.ui.fragment.PaginatedGiftListFragment;

public class MyGiftsActivity extends NavDrawerGiftListActivity {
    protected static final String TAG = MyGiftsActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(0, 0);
    }

    @Override
    protected int getActivityLayoutId() {
        return R.layout.activity_my_gifts;
    }

    @Override
    protected PaginatedGiftListFragment createListFragment() {
        MyGiftListFragment giftListFragment = new MyGiftListFragment();
        giftListFragment.setOrder(GiftOrder.ORDER_NEWEST);
        return giftListFragment;
    }

    @Override
    protected int getToolbarIcon() {
        return R.drawable.ic_drawer;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_gifts, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected int getSelfNavDrawerItem() {
        return NAVDRAWER_ITEM_MY_GIFTS;
    }
}
