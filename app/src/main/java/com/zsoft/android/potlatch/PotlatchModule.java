package com.zsoft.android.potlatch;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;

import com.zsoft.android.potlatch.rest.api.Potlatch;
import com.zsoft.android.potlatch.rest.connect.PotlatchConnectionFactory;
import com.zsoft.android.potlatch.service.RefreshRequestService;
import com.zsoft.android.potlatch.ui.activity.ChainedGiftListActivity;
import com.zsoft.android.potlatch.ui.activity.CreateGiftActivity;
import com.zsoft.android.potlatch.ui.activity.MainGiftListActivity;
import com.zsoft.android.potlatch.ui.activity.MyGiftsActivity;
import com.zsoft.android.potlatch.ui.activity.NavDrawerActivity;
import com.zsoft.android.potlatch.ui.activity.StartupActivity;
import com.zsoft.android.potlatch.ui.activity.UserGiftListActivity;
import com.zsoft.android.potlatch.ui.activity.UsersActivity;
import com.zsoft.android.potlatch.ui.activity.ViewGiftActivity;
import com.zsoft.android.potlatch.ui.fragment.ChainedGiftListFragment;
import com.zsoft.android.potlatch.ui.fragment.GiftListFragment;
import com.zsoft.android.potlatch.ui.fragment.MyGiftListFragment;
import com.zsoft.android.potlatch.ui.fragment.PaginatedGiftListFragment;
import com.zsoft.android.potlatch.ui.fragment.SignInFragment;
import com.zsoft.android.potlatch.ui.fragment.SignUpFragment;
import com.zsoft.android.potlatch.ui.fragment.UserGiftListFragment;
import com.zsoft.android.potlatch.ui.fragment.UserListFragment;

import org.springframework.security.crypto.encrypt.AndroidEncryptors;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.sqlite.SQLiteConnectionRepository;
import org.springframework.social.connect.sqlite.support.SQLiteConnectionRepositoryHelper;
import org.springframework.social.connect.support.ConnectionFactoryRegistry;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(injects = {
        StartupActivity.class,

        SignInFragment.class,
        SignUpFragment.class,
        GiftListFragment.class,
        MyGiftListFragment.class,
        ChainedGiftListFragment.class,
        UserGiftListFragment.class,
        UserListFragment.class,
        PaginatedGiftListFragment.class,

        MainGiftListActivity.class,
        ChainedGiftListActivity.class,
        MyGiftsActivity.class,
        UserGiftListActivity.class,

        ViewGiftActivity.class,
        CreateGiftActivity.class,

        NavDrawerActivity.class,
        UsersActivity.class,

        RefreshRequestService.class
})
public class PotlatchModule {
    private PotlatchApplication mApplication;

    PotlatchModule(PotlatchApplication application) {
        this.mApplication = application;
    }

    @Provides
    @Singleton
    ConnectionFactoryRegistry getConnectionFactoryRegistry(
            @InjectAndroidApplicationContext Context context) {
        ConnectionFactoryRegistry connectionFactoryRegistry = new ConnectionFactoryRegistry();
        connectionFactoryRegistry.addConnectionFactory(
                new PotlatchConnectionFactory(
                        getPotlatchBaseUrl(context),
                        getPotlatchConsumerToken(context),
                        getPotlatchConsumerTokenSecret(context)));

        return connectionFactoryRegistry;
    }

    @Provides
    @Singleton
    ConnectionRepository getConnectionRepository(
            ConnectionFactoryRegistry connectionFactoryRegistry) {
        SQLiteOpenHelper repositoryHelper = new SQLiteConnectionRepositoryHelper(mApplication);
        ConnectionRepository connectionRepository = new SQLiteConnectionRepository(repositoryHelper,
                connectionFactoryRegistry,
                AndroidEncryptors.text("password", "5c0744940b5c369b"));

        return connectionRepository;
    }

    @Provides
    @Singleton
    PotlatchConnectionFactory getPotlatchConnectionFactory(
            ConnectionFactoryRegistry connectionFactoryRegistry) {
        return (PotlatchConnectionFactory)
                connectionFactoryRegistry.getConnectionFactory(Potlatch.class);
    }

    @Provides
    @Singleton
    @InjectAndroidApplicationContext
    Context provideApplicationContext() {
        return mApplication.getApplicationContext();
    }

    private String getPotlatchBaseUrl(Context context) {
        return context.getString(R.string.potlatch_base_url);
    }

    private String getPotlatchConsumerToken(Context context) {
        return context.getString(R.string.potlatch_customer_key);
    }

    private String getPotlatchConsumerTokenSecret(Context context) {
        return context.getString(R.string.potlatch_customer_key_secret);
    }
}
