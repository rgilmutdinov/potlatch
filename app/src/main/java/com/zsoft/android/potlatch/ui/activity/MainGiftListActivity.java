package com.zsoft.android.potlatch.ui.activity;

import android.os.Bundle;
import android.view.Menu;

import com.zsoft.android.potlatch.util.GiftOrder;
import com.zsoft.android.potlatch.R;
import com.zsoft.android.potlatch.ui.fragment.GiftListFragment;
import com.zsoft.android.potlatch.ui.fragment.PaginatedGiftListFragment;

public class MainGiftListActivity extends NavDrawerGiftListActivity {
    protected static final String TAG = MainGiftListActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(0, 0);
    }

    @Override
    protected int getActivityLayoutId() {
        return R.layout.activity_main_gift_list;
    }

    @Override
    protected PaginatedGiftListFragment createListFragment() {
        GiftListFragment giftListFragment = new GiftListFragment();
        giftListFragment.setOrder(GiftOrder.ORDER_NEWEST);
        return giftListFragment;
    }

    @Override
    protected int getToolbarIcon() {
        return R.drawable.ic_drawer;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    protected int getSelfNavDrawerItem() {
        return NAVDRAWER_ITEM_BROWSE;
    }
}
