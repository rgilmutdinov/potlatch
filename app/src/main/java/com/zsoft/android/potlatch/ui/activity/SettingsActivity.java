package com.zsoft.android.potlatch.ui.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.zsoft.android.potlatch.AppPreferences;
import com.zsoft.android.potlatch.R;
import com.zsoft.android.potlatch.service.AlarmReceiver;

public class SettingsActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationIcon(R.drawable.ic_up);
        }

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PrefsFragment())
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static class PrefsFragment  extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

        public PrefsFragment() {
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            addPreferencesFromResource(R.xml.preferences);

            PreferenceManager.setDefaultValues(getActivity(), R.xml.preferences, false);
            onSharedPreferenceChanged(getPreferenceManager().getSharedPreferences(),
                    getString(R.string.key_items_per_page));
            onSharedPreferenceChanged(getPreferenceManager().getSharedPreferences(),
                    getString(R.string.key_refresh_interval));

            final EditTextPreference editItemsPerPage
                    = (EditTextPreference) getPreferenceScreen().findPreference(getString(R.string.key_items_per_page));

            editItemsPerPage.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {

                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    boolean valid = true;
                    int value;
                    try {
                        value = Integer.parseInt(newValue.toString());
                        if (value < 5 || value > 30) {
                            valid = false;
                        }
                    } catch (Exception exc) {
                        valid = false;
                    }

                    if (!valid) {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle(getActivity().getString(R.string.error_items_per_page_invalid_value_title));
                        builder.setMessage(getActivity().getString(R.string.error_items_per_page_invalid_value));
                        builder.setPositiveButton(android.R.string.ok, null);
                        builder.show();
                        return false;
                    }
                    return true;
                }
            });
        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceManager().getSharedPreferences()
                    .registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onPause() {
            getPreferenceManager().getSharedPreferences()
                    .unregisterOnSharedPreferenceChangeListener(this);
            super.onPause();
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            Context context = getActivity().getApplicationContext();

            if (keysEquals(key, R.string.key_items_per_page)) {
                int itemsPerPage = AppPreferences.getItemsPerPage(context);
                String pattern = context.getString(R.string.pattern_load_items_per_page);

                Preference pref = findPreference(key);
                pref.setSummary(String.format(pattern, itemsPerPage));
            } else if (keysEquals(key, R.string.key_auto_refresh_enabled)) {
                Boolean autoRefresh = AppPreferences.autoRefreshEnabled(context);
                if (autoRefresh) {
                    AlarmReceiver.start(context);
                } else {
                    AlarmReceiver.stop(context);
                }
            } else if (keysEquals(key, R.string.key_refresh_interval)) {
                Boolean autoRefresh = AppPreferences.autoRefreshEnabled(context);

                int refreshInterval = AppPreferences.refreshInterval(context);
                String pattern = context.getString(R.string.pattern_refresh_interval);
                Preference pref = findPreference(key);
                pref.setSummary(String.format(pattern, refreshInterval));
                if (autoRefresh) {
                    AlarmReceiver.restart(context);
                }
            }
        }

        public boolean keysEquals(String key, int resIdKey) {
            return key != null && key.equalsIgnoreCase(getString(resIdKey));
        }
    }
}
