package com.zsoft.android.potlatch.util;

public class OrderItem {
    private String mTitle;
    private int mKey;

    public OrderItem(String title, int key) {
        mTitle = title;
        mKey = key;
    }

    public String getTitle() {
        return mTitle;
    }

    public int getKey() {
        return mKey;
    }
}
