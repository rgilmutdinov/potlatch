package com.zsoft.android.potlatch.rest.api.impl;

import com.google.common.base.Joiner;
import com.zsoft.android.potlatch.model.Gift;
import com.zsoft.android.potlatch.model.GiftForm;
import com.zsoft.android.potlatch.rest.api.GiftOperations;

import org.springframework.core.io.FileSystemResource;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Arrays;
import java.util.List;

public class GiftTemplate extends AbstractPotlatchOperations implements GiftOperations {
    private static final String KEY_IMAGE_FILE = "imageFile";
    private static final String KEY_GIFT_FORM  = "giftForm";

    public GiftTemplate(final String baseUrl, final String accessToken) {
        super(baseUrl, accessToken);
    }

    @Override
    public List<Gift> getGifts(boolean includeNegativeRated, int page, int pageSize) {
        ResponseEntity<Gift[]> entity =
                getRestTemplate().getForEntity(buildUri("/gifts?p={p}&l={l}&obs={obs}"),
                        Gift[].class, page, pageSize, includeNegativeRated);

        return Arrays.asList(entity.getBody());
    }

    @Override
    public List<Gift> getGifts(boolean includeNegativeRated, int page, int pageSize,
                               String orderBy, boolean ascendingOrder) {
        ResponseEntity<Gift[]> entity =
                getRestTemplate().getForEntity(buildUri("/gifts?p={p}&l={l}&s={s}&d={d}&obs={obs}"),
                        Gift[].class, page, pageSize, orderBy, ascendingOrder ? "asc" : "desc",
                        includeNegativeRated);

        return Arrays.asList(entity.getBody());
    }

    @Override
    public List<Gift> getMyGifts(int page, int pageSize, String orderBy, boolean ascendingOrder) {
        ResponseEntity<Gift[]> entity =
                getRestTemplate().getForEntity(buildUri("/users/me/gifts?p={p}&l={l}&s={s}&d={d}"),
                        Gift[].class, page, pageSize, orderBy, ascendingOrder ? "asc" : "desc");

        return Arrays.asList(entity.getBody());
    }

    @Override
    public List<Gift> getGiftsOfUser(long ownerId, boolean includeNegativeRated, int page,
                                     int pageSize, String orderBy, boolean ascendingOrder) {
        ResponseEntity<Gift[]> entity =
                getRestTemplate().getForEntity(
                        buildUri("/users/{ownerId}/gifts?p={p}&l={l}&s={s}&d={d}&obs={obs}"),
                        Gift[].class, ownerId, page, pageSize, orderBy,
                        ascendingOrder ? "asc" : "desc", includeNegativeRated);

        return Arrays.asList(entity.getBody());
    }

    @Override
    public List<Gift> getChainedGifts(long parentId, boolean includeNegativeRated, int page,
                                      int pageSize) {
        ResponseEntity<Gift[]> entity =
                getRestTemplate().getForEntity(buildUri("/gifts/{parentId}/chain?p={p}&l={l}&obs={obs}"),
                        Gift[].class, parentId, page, pageSize, includeNegativeRated);

        return Arrays.asList(entity.getBody());
    }

    @Override
    public List<Gift> getChainedGifts(long parentId, boolean includeNegativeRated, int page,
                                      int pageSize, String orderBy, boolean ascendingOrder) {
        ResponseEntity<Gift[]> entity =
                getRestTemplate().getForEntity(
                        buildUri("/gifts/{parentId}/chain?p={p}&l={l}&s={s}&d={d}&obs={obs}"),
                        Gift[].class, parentId, page, pageSize, orderBy,
                        ascendingOrder ? "asc" : "desc", includeNegativeRated);

        return Arrays.asList(entity.getBody());
    }

    @Override
    public List<Gift> searchGifts(String query, boolean includeNegativeRated, int page, int pageSize) {
        ResponseEntity<Gift[]> entity =
                getRestTemplate().getForEntity(buildUri("/gifts/search?q={q}&p={p}&l={l}&obs={obs}"),
                        Gift[].class, query, page, pageSize, includeNegativeRated);

        return Arrays.asList(entity.getBody());
    }

    @Override
    public List<Gift> searchGifts(String query, boolean includeNegativeRated, int page,
                                  int pageSize, String orderBy, boolean ascendingOrder) {
        ResponseEntity<Gift[]> entity =
                getRestTemplate().getForEntity(
                        buildUri("/gifts/search?q={q}&p={p}&l={l}&s={s}&d={d}&obs={obs}"),
                        Gift[].class, query, page, pageSize, orderBy,
                        ascendingOrder ? "asc" : "desc", includeNegativeRated);

        return Arrays.asList(entity.getBody());
    }

    @Override
    public List<Gift> searchMyGifts(String query, int page, int pageSize, String orderBy,
                                    boolean ascendingOrder) {
        ResponseEntity<Gift[]> entity =
                getRestTemplate().getForEntity(
                        buildUri("/users/me/gifts/search?q={q}&p={p}&l={l}&s={s}&d={d}"),
                        Gift[].class, query, page, pageSize,
                        orderBy, ascendingOrder ? "asc" : "desc");

        return Arrays.asList(entity.getBody());
    }

    @Override
    public List<Gift> searchGiftsOfUser(long ownerId, String query, boolean includeNegativeRated,
                                        int page, int pageSize, String orderBy, boolean ascendingOrder) {
        ResponseEntity<Gift[]> entity =
                getRestTemplate().getForEntity(
                        buildUri("/users/{ownerId}/gifts/search?q={q}&p={p}&l={l}&s={s}&d={d}&obs={obs}"),
                        Gift[].class, ownerId, query, page, pageSize, orderBy,
                        ascendingOrder ? "asc" : "desc", includeNegativeRated);

        return Arrays.asList(entity.getBody());
    }

    @Override
    public List<Gift> searchChainedGifts(long parentId, String query,
                                         boolean includeNegativeRated, int page, int pageSize) {
        ResponseEntity<Gift[]> entity =
                getRestTemplate().getForEntity(
                        buildUri("/gifts/{parentId}/search?q={q}&p={p}&l={l}&obs={obs}"),
                        Gift[].class, parentId, query, page, pageSize, includeNegativeRated);

        return Arrays.asList(entity.getBody());
    }

    @Override
    public List<Gift> searchChainedGifts(long parentId, String query, boolean includeNegativeRated,
                                         int page, int pageSize, String orderBy, boolean ascendingOrder) {
        ResponseEntity<Gift[]> entity =
                getRestTemplate().getForEntity(
                        buildUri("/gifts/{parentId}/search?q={q}&p={p}&l={l}&s={s}&d={d}&obs={obs}"),
                        Gift[].class, parentId, query, page,
                        pageSize, orderBy, ascendingOrder ? "asc" : "desc", includeNegativeRated);

        return Arrays.asList(entity.getBody());
    }

    @Override
    public List<Gift> refreshGifts(List<Long> ids) {
        String sids = Joiner.on(',').join(ids);
        ResponseEntity<Gift[]> entity =
                getRestTemplate().getForEntity(buildUri("/gifts/refresh/{ids}"), Gift[].class, sids);
        return Arrays.asList(entity.getBody());
    }

    @Override
    public Gift getGift(long id) {
        ResponseEntity<Gift> entity =
                getRestTemplate().getForEntity(buildUri("/gifts/{id}"), Gift.class, id);
        return entity.getBody();
    }

    @Override
    public boolean deleteGift(long id) {
        getRestTemplate().delete(buildUri("/gifts/{id}"), id);
        return true;
    }

    @Override
    public Gift addGift(GiftForm giftForm, String imagePath) {
        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();
        parts.add(KEY_IMAGE_FILE, new FileSystemResource(imagePath));
        parts.add(KEY_GIFT_FORM, giftForm);

        ResponseEntity<Gift> giftEntity =
                getRestTemplate().postForEntity(buildUri("/gifts"), parts, Gift.class);

        return giftEntity.getBody();
    }

    @Override
    public Gift giftVoteUp(long id) {
        ResponseEntity<Gift> entity =
                getRestTemplate().postForEntity(
                        buildUri("/gifts/{id}/voteUp"), null, Gift.class, id);
        return entity.getBody();
    }

    @Override
    public Gift giftVoteDown(long id) {
        ResponseEntity<Gift> entity =
                getRestTemplate().postForEntity(
                        buildUri("/gifts/{id}/voteDown"), null, Gift.class, id);
        return entity.getBody();
    }
}
