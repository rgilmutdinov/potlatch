package com.zsoft.android.potlatch;

public class Constants {
    public static final String EXTRA_GIFT = "extra_gift";
    public static final String EXTRA_USER = "extra_user";

    public static final String ALARMID_REFRESH_DATA = "com.zsoft.android.potlatch.refresh";
}
