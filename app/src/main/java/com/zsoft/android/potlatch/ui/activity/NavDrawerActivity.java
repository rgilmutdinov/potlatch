package com.zsoft.android.potlatch.ui.activity;

import android.accounts.Account;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zsoft.android.potlatch.DaggerInjectionUtils;
import com.zsoft.android.potlatch.R;
import com.zsoft.android.potlatch.rest.connect.PotlatchConnectionFactory;
import com.zsoft.android.potlatch.util.AccountUtils;
import com.zsoft.android.potlatch.util.HelpUtils;
import com.zsoft.android.potlatch.util.LoginAndAuthHelper;

import org.springframework.social.connect.ConnectionRepository;

import java.util.ArrayList;

import javax.inject.Inject;

public class NavDrawerActivity extends ActionBarActivity {
    protected static final String TAG = NavDrawerActivity.class.getSimpleName();

    @Inject
    ConnectionRepository mConnectionRepository;

    @Inject
    PotlatchConnectionFactory mConnectionFactory;

    private DrawerLayout mDrawerLayout;
    private ViewGroup mDrawerItemsListContainer;

    private View grpProfileContent;
    private TextView mTxtUsername;
    private TextView mTxtUsermail;
    private ImageView mImgUserImage;
    private ProgressBar mProgressBar;

    private Handler mHandler;

    private LoginAndAuthHelper mLoginAndAuthHelper;

    protected static final int NAVDRAWER_ITEM_BROWSE     = 0;
    protected static final int NAVDRAWER_ITEM_MY_GIFTS   = 1;
    protected static final int NAVDRAWER_ITEM_USERS = 2;
    protected static final int NAVDRAWER_ITEM_SETTINGS   = 3;
    protected static final int NAVDRAWER_ITEM_DISCONNECT = 4;
    protected static final int NAVDRAWER_ITEM_INVALID    = -1;
    protected static final int NAVDRAWER_ITEM_SEPARATOR  = -2;

    // delay to launch nav drawer item, to allow close animation to play
    private static final int NAVDRAWER_LAUNCH_DELAY = 250;

    // fade in and fade out durations for the main content when switching between
    // different Activities of the app through the Nav Drawer
    private static final int MAIN_CONTENT_FADEOUT_DURATION = 150;
    private static final int MAIN_CONTENT_FADEIN_DURATION = 250;

    // titles for navdrawer items (indices must correspond to the above)
    private static final int[] NAVDRAWER_TITLE_RES_ID = new int[]{
            R.string.navdrawer_item_browse,
            R.string.navdrawer_item_my_gifts,
            R.string.navdrawer_item_givers,
            R.string.navdrawer_item_settings,
            R.string.navdrawer_item_disconnect
    };

    // icons for navdrawer items (indices must correspond to above array)
    private static final int[] NAVDRAWER_ICON_RES_ID = new int[] {
            R.drawable.ic_browse,
            R.drawable.ic_my_gifts,
            R.drawable.ic_top_givers,
            R.drawable.ic_settings,
            R.drawable.ic_logout
    };

    // list of navdrawer items that were actually added to the navdrawer, in order
    private ArrayList<Integer> mNavDrawerItems = new ArrayList<Integer>();

    // views that correspond to each navdrawer item, null if not yet created
    private View[] mNavDrawerItemViews = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerInjectionUtils.inject(this);

        mHandler = new Handler();

        overridePendingTransition(0, 0);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        mLoginAndAuthHelper = new LoginAndAuthHelper(this);

        setupNavDrawer();
    }

    private void setupAccountBox() {
        mTxtUsername  = (TextView) findViewById(R.id.txtUsername);
        mTxtUsermail  = (TextView) findViewById(R.id.txtEmail);
        mImgUserImage = (ImageView) findViewById(R.id.imgUserImage);

        grpProfileContent = findViewById(R.id.grpProfileContent);
        mProgressBar  = (ProgressBar) findViewById(R.id.progressBar);

        Account chosenAccount = AccountUtils.getActiveAccount(this);
        if (chosenAccount == null) {
            // No account logged in; hide account box
            grpProfileContent.setVisibility(View.INVISIBLE);
            mProgressBar.setVisibility(View.INVISIBLE);
        } else {
            String username = AccountUtils.getName(this);

            mTxtUsername.setText(username);
            mTxtUsermail.setText(AccountUtils.getEmail(this));

            String url = AccountUtils.getImageUrl(this);
            if (url != null) {
                url += "?r=g&d=identicon";
                Picasso.with(this)
                        .load(url)
                        .error(R.drawable.account)
                        .fit()
                        .centerInside()
                        .noFade()
                        .into(mImgUserImage);
                mImgUserImage.setContentDescription(username);
            } else {
                Picasso.with(this).cancelRequest(mImgUserImage);
                mImgUserImage.setImageDrawable(null);
                mImgUserImage.setContentDescription(null);
            }

            grpProfileContent.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.INVISIBLE);
        }
    }

    /** Populates the navigation drawer with the appropriate items. */
    private void setupNavDrawer() {
        int selfItem = getSelfNavDrawerItem();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (mDrawerLayout == null) {
            return;
        }

        View navDrawer = mDrawerLayout.findViewById(R.id.navdrawer);
        if (selfItem == NAVDRAWER_ITEM_INVALID) {
            // do not show a nav drawer
            if (navDrawer != null) {
                ((ViewGroup) navDrawer.getParent()).removeView(navDrawer);
            }
            mDrawerLayout = null;
            return;
        }

        setupAccountBox();

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, Gravity.START);

        mNavDrawerItems.clear();
        mNavDrawerItems.add(NAVDRAWER_ITEM_BROWSE);
        mNavDrawerItems.add(NAVDRAWER_ITEM_MY_GIFTS);

        mNavDrawerItems.add(NAVDRAWER_ITEM_SEPARATOR);

        mNavDrawerItems.add(NAVDRAWER_ITEM_USERS);

        mNavDrawerItems.add(NAVDRAWER_ITEM_SEPARATOR);

        mNavDrawerItems.add(NAVDRAWER_ITEM_SETTINGS);
        mNavDrawerItems.add(NAVDRAWER_ITEM_DISCONNECT);

        createNavDrawerItems();
    }

    private void createNavDrawerItems() {
        mDrawerItemsListContainer = (ViewGroup) findViewById(R.id.navdrawer_items_list);
        if (mDrawerItemsListContainer == null) {
            return;
        }

        mNavDrawerItemViews = new View[mNavDrawerItems.size()];
        mDrawerItemsListContainer.removeAllViews();
        int i = 0;
        for (int itemId : mNavDrawerItems) {
            mNavDrawerItemViews[i] = makeNavDrawerItem(itemId, mDrawerItemsListContainer);
            mDrawerItemsListContainer.addView(mNavDrawerItemViews[i]);
            ++i;
        }
    }

    private View makeNavDrawerItem(final int itemId, ViewGroup container) {
        boolean selected = getSelfNavDrawerItem() == itemId;
        int layoutToInflate;
        if (itemId == NAVDRAWER_ITEM_SEPARATOR) {
            layoutToInflate = R.layout.navdrawer_separator;
        } else {
            layoutToInflate = R.layout.navdrawer_item;
        }
        View view = getLayoutInflater().inflate(layoutToInflate, container, false);

        if (isSeparator(itemId)) {
            // we are done
            setAccessibilityIgnore(view);
            return view;
        }

        ImageView iconView = (ImageView) view.findViewById(R.id.icon);
        TextView titleView = (TextView) view.findViewById(R.id.title);
        int iconId = itemId >= 0 && itemId < NAVDRAWER_ICON_RES_ID.length ?
                NAVDRAWER_ICON_RES_ID[itemId] : 0;
        int titleId = itemId >= 0 && itemId < NAVDRAWER_TITLE_RES_ID.length ?
                NAVDRAWER_TITLE_RES_ID[itemId] : 0;

        // set icon and text
        iconView.setVisibility(iconId > 0 ? View.VISIBLE : View.GONE);
        if (iconId > 0) {
            iconView.setImageResource(iconId);
        }
        titleView.setText(getString(titleId));

        formatNavDrawerItem(view, itemId, selected);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNavDrawerItemClicked(itemId);
            }
        });

        return view;
    }

    private void formatNavDrawerItem(View view, int itemId, boolean selected) {
        if (isSeparator(itemId)) {
            // not applicable
            return;
        }

        ImageView iconView = (ImageView) view.findViewById(R.id.icon);
        TextView titleView = (TextView) view.findViewById(R.id.title);

        // configure its appearance according to whether or not it's selected
        titleView.setTextColor(selected ?
                getResources().getColor(R.color.colorPrimaryDark) :
                getResources().getColor(R.color.abc_primary_text_material_light));
        iconView.setColorFilter(selected ?
                getResources().getColor(R.color.colorPrimaryDark) :
                getResources().getColor(R.color.primary_dark_material_light));
    }

    public static void setAccessibilityIgnore(View view) {
        view.setClickable(false);
        view.setFocusable(false);
        view.setContentDescription("");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            view.setImportantForAccessibility(View.IMPORTANT_FOR_ACCESSIBILITY_NO);
        }
    }

    private void onNavDrawerItemClicked(final int itemId) {
        if (itemId == getSelfNavDrawerItem()) {
            mDrawerLayout.closeDrawer(Gravity.START);
            return;
        }

        if (isSpecialItem(itemId)) {
            goToNavDrawerItem(itemId);
        } else {
            // launch the target Activity after a short delay, to allow the close animation to play
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    goToNavDrawerItem(itemId);
                }
            }, NAVDRAWER_LAUNCH_DELAY);

            // change the active item on the list so the user can see the item changed
            setSelectedNavDrawerItem(itemId);
            // fade out the main content
            View mainContent = findViewById(R.id.main_content);
            if (mainContent != null) {
                mainContent.animate().alpha(0).setDuration(MAIN_CONTENT_FADEOUT_DURATION);
            }
        }

        mDrawerLayout.closeDrawer(Gravity.START);
    }

    private void goToNavDrawerItem(int item) {
        Intent intent;
        switch (item) {
            case NAVDRAWER_ITEM_BROWSE:
                intent = new Intent(this, MainGiftListActivity.class);
                startActivity(intent);
                finish();
                break;
            case NAVDRAWER_ITEM_MY_GIFTS:
                intent = new Intent(this, MyGiftsActivity.class);
                startActivity(intent);
                finish();
                break;
            case NAVDRAWER_ITEM_USERS:
                intent = new Intent(this, UsersActivity.class);
                startActivity(intent);
                finish();
                break;
            case NAVDRAWER_ITEM_DISCONNECT:
                disconnect();
                break;
            case NAVDRAWER_ITEM_SETTINGS:
                intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;
        }
    }

    /**
     * Sets up the given navdrawer item's appearance to the selected state. Note: this could
     * also be accomplished (perhaps more cleanly) with state-based layouts.
     */
    private void setSelectedNavDrawerItem(int itemId) {
        if (mNavDrawerItemViews != null) {
            for (int i = 0; i < mNavDrawerItemViews.length; i++) {
                if (i < mNavDrawerItems.size()) {
                    int thisItemId = mNavDrawerItems.get(i);
                    formatNavDrawerItem(mNavDrawerItemViews[i], thisItemId, itemId == thisItemId);
                }
            }
        }
    }

    protected int getSelfNavDrawerItem() {
        return NAVDRAWER_ITEM_INVALID;
    }

    private boolean isSpecialItem(int itemId) {
        return itemId == NAVDRAWER_ITEM_SETTINGS;
    }

    private boolean isSeparator(int itemId) {
        return itemId == NAVDRAWER_ITEM_SEPARATOR;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch(id) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(Gravity.START);
                return true;
            case R.id.action_about:
                HelpUtils.showAbout(this);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void disconnect() {
        mConnectionRepository.removeConnections(mConnectionFactory.getProviderId());
        mLoginAndAuthHelper.logout();

        startActivity(new Intent(this, ConnectionActivity.class));
        finish();
    }


    @Override
    public void onBackPressed() {
        if (isNavDrawerOpen()) {
            closeNavDrawer();
        } else {
            super.onBackPressed();
        }
    }

    protected boolean isNavDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(Gravity.START);
    }

    protected void closeNavDrawer() {
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(Gravity.START);
        }
    }
}
