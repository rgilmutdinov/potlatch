package com.zsoft.android.potlatch.rest.api;

import org.springframework.web.client.RestTemplate;

public interface Potlatch {
    public static final String PROVIDER_ID = "potlatch";

    UserOperations userOperations();

    RestTemplate restOperations();

    GiftOperations giftOperations();
}
