package com.zsoft.android.potlatch.rest.api;

import com.zsoft.android.potlatch.model.User;

import java.util.List;

public interface UserOperations {
    User getProfile();

    List<User> searchUsers(String query, int pageId, int pageSize, String orderBy, boolean ascendingOrder);

    List<User> getAllUsers(int pageId, int pageSize, String orderBy, boolean ascendingOrder);
}
