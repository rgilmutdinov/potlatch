package com.zsoft.android.potlatch.model;

import com.google.common.base.Objects;

import java.util.Calendar;

public class Gift extends Entity {
    private Long parentId;

    private String title;
    private String text;
    
    private Calendar created;
    private String url;
    
    private long views;
    private long rating;
    private long age;

    private String ownerName;
    private Long ownerId;

    private int vote;

    public Gift() {
        
    }

    public Long getParentId() {
        return parentId;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public Calendar getCreated() {
        return created;
    }

    public String getUrl() {
        return url;
    }

    public long getViews() {
        return views;
    }

    public long getRating() {
        return rating;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setCreated(Calendar created) {
        this.created = created;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setViews(long views) {
        this.views = views;
    }

    public void setRating(long rating) {
        this.rating = rating;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public int getVote() {
        return vote;
    }

    public void setVote(int vote) {
        this.vote = vote;
    }

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Gift) {
            Gift other = (Gift) o;
            return Objects.equal(id, other.id)
                    && Objects.equal(parentId, other.parentId)
                    && Objects.equal(title, other.title)
                    && Objects.equal(text, other.text)
                    && Objects.equal(created, other.created);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, parentId, title, text, created);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("id", id)
                .add("parentId", parentId)
                .add("title", title)
                .add("text", text)
                .add("created", created)
                .toString();
    }
}
