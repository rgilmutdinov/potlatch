package com.zsoft.android.potlatch.ui.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;

public class AbstractDialogFragment extends Fragment {
    protected static final String TAG = AbstractDialogFragment.class.getSimpleName();

    private ProgressDialog mProgressDialog;

    private boolean mDestroyed = false;

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mDestroyed = true;
    }

    public void showProgressDialog(CharSequence message) {
        if (this.mProgressDialog == null) {
            this.mProgressDialog = new ProgressDialog(getActivity());
            this.mProgressDialog.setIndeterminate(true);
        }

        this.mProgressDialog.setMessage(message);
        this.mProgressDialog.setCancelable(false);
        this.mProgressDialog.show();
    }

    public void dismissProgressDialog() {
        if (this.mProgressDialog != null && !this.mDestroyed) {
            this.mProgressDialog.dismiss();
        }
    }

    protected void showMessage(String title, String message) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle(title);
        alertDialog.setCancelable(true);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.setMessage(message);
        alertDialog.show();
    }
}
