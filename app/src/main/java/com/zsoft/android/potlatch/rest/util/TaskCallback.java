package com.zsoft.android.potlatch.rest.util;

public interface TaskCallback<T> {
    public void success(T result);
    public void error(Exception e);
}
