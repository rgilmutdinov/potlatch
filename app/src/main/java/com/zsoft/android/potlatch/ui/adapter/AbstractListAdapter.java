package com.zsoft.android.potlatch.ui.adapter;

import android.content.Context;
import android.widget.BaseAdapter;

import com.zsoft.android.potlatch.model.Entity;

import java.util.List;

public abstract class AbstractListAdapter<T extends Entity> extends BaseAdapter {
    protected static final String TAG = AbstractListAdapter.class.getSimpleName();

    protected final Context mContext;
    protected List<T> mItems;

    public AbstractListAdapter(Context context) {
        this.mContext = context;
    }

    public void setItems(List<T> items) {
        this.mItems = items;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mItems != null ? mItems.size() : 0;
    }

    @Override
    public T getItem(int position) {
        if (mItems != null && position >= 0 && position < getCount()) {
            return mItems.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (isEmpty()) {
            return -1;
        }
        return getItem(position).getId();
    }
}
