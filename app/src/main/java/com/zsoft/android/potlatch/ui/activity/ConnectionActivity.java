package com.zsoft.android.potlatch.ui.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.zsoft.android.potlatch.R;
import com.zsoft.android.potlatch.ui.fragment.SignInFragment;
import com.zsoft.android.potlatch.ui.fragment.SignUpFragment;
import com.zsoft.android.potlatch.ui.fragment.SplashFragment;
import com.zsoft.android.potlatch.ui.widget.SlidingTabLayout;

import java.util.ArrayList;
import java.util.List;

public class ConnectionActivity extends ActionBarActivity {
    private SlidingTabLayout mSlidingTabLayout;
    private ViewPager mViewPager;
    private List<PagerItem> mTabs = new ArrayList<PagerItem>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection);

        int colorAccent = getResources().getColor(R.color.colorAccent);
        mTabs.add(new PagerItem(
                SplashFragment.class,
                getString(R.string.tab_logo), // Title
                colorAccent, // Indicator color
                Color.TRANSPARENT // Divider color
        ));

        mTabs.add(new PagerItem(
                SignInFragment.class,
                getString(R.string.tab_sign_in), // Title
                colorAccent, // Indicator color
                Color.GRAY // Divider color
        ));

        mTabs.add(new PagerItem(
                SignUpFragment.class,
                getString(R.string.tab_sign_up), // Title
                colorAccent, // Indicator color
                Color.TRANSPARENT // Divider color
        ));
        
        // Get the ViewPager and set it's PagerAdapter so that it can display items
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mViewPager.setAdapter(new SampleFragmentPagerAdapter(getSupportFragmentManager()));

        // Give the SlidingTabLayout the ViewPager, this must be done AFTER the ViewPager has had
        // it's PagerAdapter set.
        mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setCustomTabView(R.layout.tab_indicator, android.R.id.text1);
        mSlidingTabLayout.setViewPager(mViewPager);

        mSlidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

            @Override
            public int getIndicatorColor(int position) {
                return mTabs.get(position).getIndicatorColor();
            }

            @Override
            public int getDividerColor(int position) {
                return mTabs.get(position).getDividerColor();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class SampleFragmentPagerAdapter extends FragmentPagerAdapter {
        SampleFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Class fragmentClass = mTabs.get(i).getFragmentClass();
            return Fragment.instantiate(ConnectionActivity.this, fragmentClass.getName());
        }

        @Override
        public int getCount() {
            return mTabs.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTabs.get(position).getTitle();
        }
    }

    static class PagerItem {
        private final Class mFragmentClass;
        private final CharSequence mTitle;
        private final int mIndicatorColor;
        private final int mDividerColor;

        PagerItem(Class fragmentClass, CharSequence title, int indicatorColor, int dividerColor) {
            mFragmentClass = fragmentClass;
            mTitle = title;
            mIndicatorColor = indicatorColor;
            mDividerColor = dividerColor;
        }

        CharSequence getTitle() {
            return mTitle;
        }

        int getIndicatorColor() {
            return mIndicatorColor;
        }

        int getDividerColor() {
            return mDividerColor;
        }

        Class getFragmentClass() {
            return mFragmentClass;
        }
    }
}
