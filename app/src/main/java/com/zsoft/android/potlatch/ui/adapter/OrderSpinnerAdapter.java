package com.zsoft.android.potlatch.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.zsoft.android.potlatch.R;
import com.zsoft.android.potlatch.util.OrderItem;

import java.util.ArrayList;

public class OrderSpinnerAdapter extends BaseAdapter {
    private ArrayList<OrderItem> mItems;

    private final Context mContext;

    public OrderSpinnerAdapter(Context context, ArrayList<OrderItem> items) {
        this(context);

        mItems = items;
    }

    public OrderSpinnerAdapter(Context context) {
        this.mContext = context;

        mItems = new ArrayList<OrderItem>();
    }

    public void clear() {
        mItems.clear();
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getDropDownView(int position, View view, ViewGroup parent) {
        if (view == null || !view.getTag().toString().equals("DROPDOWN")) {
            LayoutInflater li = LayoutInflater.from(mContext);
            view = li.inflate(R.layout.actionbar_spinner_item_dropdown,
                    parent, false);
            view.setTag("DROPDOWN");
        }

        TextView textView = (TextView) view.findViewById(android.R.id.text1);
        textView.setText(getTitle(position));

        return view;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null || !view.getTag().toString().equals("NON_DROPDOWN")) {
            LayoutInflater li = LayoutInflater.from(mContext);
            view = li.inflate(R.layout.actionbar_spinner_item,
                    parent, false);
            view.setTag("NON_DROPDOWN");
        }

        TextView textView = (TextView) view.findViewById(android.R.id.text1);
        textView.setText(getTitle(position));

        return view;
    }

    private String getTitle(int position) {
        return position >= 0 && position < mItems.size() ? mItems.get(position).getTitle() : "";
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }
}
