package com.zsoft.android.potlatch.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.dd.processbutton.FlatButton;
import com.zsoft.android.potlatch.DaggerInjectionUtils;
import com.zsoft.android.potlatch.R;
import com.zsoft.android.potlatch.model.UserForm;
import com.zsoft.android.potlatch.rest.RestTemplateUtils;
import com.zsoft.android.potlatch.model.User;
import com.zsoft.android.potlatch.rest.util.CallableTask;
import com.zsoft.android.potlatch.rest.util.TaskCallback;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.Callable;

public class SignUpFragment extends AbstractDialogFragment {
    private FlatButton mBtnSignUp;
    private EditText mTxtUsername;
    private EditText mTxtEmail;
    private EditText mTxtPassword;

    public SignUpFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerInjectionUtils.inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View top = inflater.inflate(R.layout.fragment_sign_up, container, false);

        mBtnSignUp   = (FlatButton) top.findViewById(R.id.btnSignUp);
        mTxtUsername = (EditText) top.findViewById(R.id.txtUsername);
        mTxtEmail    = (EditText) top.findViewById(R.id.txtEmail);
        mTxtPassword = (EditText) top.findViewById(R.id.txtPassword);

        mBtnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String username = mTxtUsername.getText().toString();
                final String email    = mTxtEmail.getText().toString();
                final String password = mTxtPassword.getText().toString();

                enableControls(false);
                showProgressDialog(getString(R.string.account_dialog_creating));

                CallableTask.invoke(
                        new Callable<Boolean>() {
                            @Override
                            public Boolean call() throws Exception {
                                RestTemplate template = RestTemplateUtils
                                        .createUnsafeTemplate();
                                UserForm userForm = new UserForm(
                                        username,
                                        email,
                                        password
                                );

                                String baseUrl = getString(R.string.potlatch_base_url);
                                ResponseEntity<User> entity = template.postForEntity(
                                        baseUrl + "/users", userForm, User.class);
                                return entity.hasBody();
                            }
                        },
                        new TaskCallback<Boolean>() {
                            @Override
                            public void success(Boolean success) {
                                dismissProgressDialog();
                                if (success) {
                                    showMessage(getString(R.string.account_dialog_success),
                                            getString(R.string.account_dialog_created));
                                } else {
                                    showMessage(getString(R.string.account_error_registration),
                                            getString(R.string.account_error_unknown));
                                }
                                enableControls(true);
                            }

                            @Override
                            public void error(Exception e) {
                                enableControls(true);
                                dismissProgressDialog();
                                showMessage(getString(R.string.account_error_registration),
                                        e.getLocalizedMessage());
                            }
                        }
                );
            }
        });
        return top;
    }

    private void enableControls(boolean enable) {
        mBtnSignUp.setEnabled(enable);
        mTxtUsername.setEnabled(enable);
        mTxtEmail.setEnabled(enable);
        mTxtPassword.setEnabled(enable);
    }
}
