package com.zsoft.android.potlatch.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dd.processbutton.FlatButton;
import com.zsoft.android.potlatch.DaggerInjectionUtils;
import com.zsoft.android.potlatch.R;
import com.zsoft.android.potlatch.rest.api.Potlatch;
import com.zsoft.android.potlatch.rest.connect.PotlatchConnectionFactory;
import com.zsoft.android.potlatch.rest.util.CallableTask;
import com.zsoft.android.potlatch.rest.util.TaskCallback;
import com.zsoft.android.potlatch.ui.activity.MainGiftListActivity;
import com.zsoft.android.potlatch.util.LoginAndAuthHelper;

import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.DuplicateConnectionException;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.social.oauth2.OAuth2Operations;

import java.util.concurrent.Callable;

import javax.inject.Inject;

public class SignInFragment extends AbstractDialogFragment {
    private static final String TAG = SignInFragment.class.getSimpleName();

    @Inject
    ConnectionRepository mConnectionRepository;

    @Inject
    PotlatchConnectionFactory mConnectionFactory;

    private FlatButton mBtnSignIn;
    private TextView mTxtUsername;
    private TextView mTxtPassword;

    public SignInFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerInjectionUtils.inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View top = inflater.inflate(R.layout.fragment_sign_in, container, false);

        mTxtUsername = (TextView) top.findViewById(R.id.txtUsername);
        mTxtPassword = (TextView) top.findViewById(R.id.txtPassword);

        mBtnSignIn = (FlatButton) top.findViewById(R.id.btnSignIn);
        mBtnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String username = mTxtUsername.getText().toString();
                final String password = mTxtPassword.getText().toString();

                enableControls(false);

                login(username, password);
            }
        });

        return top;
    }

    private void login(final String username, final String password) {
        showProgressDialog(getString(R.string.account_dialog_logging));
        CallableTask.invoke(
                new Callable<AccessGrant>() {
                    @Override
                    public AccessGrant call() throws Exception {
                        OAuth2Operations oauth = mConnectionFactory.getOAuthOperations();
                        return oauth.exchangeCredentialsForAccess(username, password, null);
                    }
                },
                new TaskCallback<AccessGrant>() {
                    @Override
                    public void success(AccessGrant accessGrant) {
                        createConnection(accessGrant);
                    }

                    @Override
                    public void error(Exception e) {
                        enableControls(true);
                        dismissProgressDialog();
                        showMessage(getString(R.string.account_error_login),
                                e.getLocalizedMessage());
                    }
                });
    }

    private void createConnection(final AccessGrant accessGrant) {
        CallableTask.invoke(
                new Callable<Boolean>() {
                    @Override
                    public Boolean call() throws Exception {
                        Connection<Potlatch> connection = mConnectionFactory
                                .createConnection(accessGrant);
                        try {
                            // persist connection to the repository
                            Connection<Potlatch> savedConnection =
                                    mConnectionRepository.findPrimaryConnection(Potlatch.class);
                            if (savedConnection == null) {
                                mConnectionRepository.addConnection(connection);
                            } else {
                                mConnectionRepository.updateConnection(connection);
                            }

                            new LoginAndAuthHelper(getActivity())
                                    .onLogin(connection.getApi().userOperations().getProfile());

                            return true;
                        } catch (DuplicateConnectionException e) {
                            // connection already exists in repository!
                            Log.d(TAG, e.getLocalizedMessage(), e);
                        }
                        return false;
                    }
                },
                new TaskCallback<Boolean>() {
                    @Override
                    public void success(Boolean connectionCreated) {
                        enableControls(true);
                        dismissProgressDialog();
                        if (connectionCreated) {
                            startMainActivity();
                        } else {
                            showMessage(getString(R.string.account_error_login),
                                    getString(R.string.account_error_unknown));
                        }
                    }

                    @Override
                    public void error(Exception e) {
                        enableControls(true);
                        dismissProgressDialog();
                        showMessage(getString(R.string.account_error_login),
                                e.getLocalizedMessage());
                    }
                });
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void startMainActivity() {
        startActivity(new Intent(getActivity(), MainGiftListActivity.class));
        getActivity().finish();
    }

    private void enableControls(boolean enable) {
        mBtnSignIn.setEnabled(enable);
        mTxtUsername.setEnabled(enable);
        mTxtPassword.setEnabled(enable);
    }
}
