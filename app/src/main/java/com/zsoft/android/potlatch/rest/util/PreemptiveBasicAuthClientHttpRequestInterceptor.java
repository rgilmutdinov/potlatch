package com.zsoft.android.potlatch.rest.util;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.security.crypto.codec.Base64;

import java.io.IOException;
import java.nio.charset.Charset;

/**
 * Client request interceptor that does preemptive HTTP Basic authentication by ensuring that an Authorization
 * header with HTTP Basic credentials is always included in the request headers.
 * @author Craig Walls
 */
public class PreemptiveBasicAuthClientHttpRequestInterceptor
        implements ClientHttpRequestInterceptor {

    private final String mUsername;
    private final String mPassword;
    private final Charset mCharset;

    public PreemptiveBasicAuthClientHttpRequestInterceptor(
            String username, String password) {
        this(username, password, Charset.forName("UTF-8"));
    }

    public PreemptiveBasicAuthClientHttpRequestInterceptor(
            String username, String password, Charset charset) {
        this.mUsername = username;
        this.mPassword = password;
        this.mCharset = charset;
    }

    public ClientHttpResponse intercept(
            HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        request.getHeaders().set("Authorization",
                "Basic " + new String(Base64.encode((mUsername + ":" + mPassword)
                        .getBytes(mCharset)), mCharset));
        return execution.execute(request, body);
    }

}