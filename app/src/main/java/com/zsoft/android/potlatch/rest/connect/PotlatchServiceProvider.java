package com.zsoft.android.potlatch.rest.connect;

import com.zsoft.android.potlatch.rest.api.Potlatch;
import com.zsoft.android.potlatch.rest.api.impl.PotlatchTemplate;

import org.springframework.social.oauth2.AbstractOAuth2ServiceProvider;

public class PotlatchServiceProvider extends AbstractOAuth2ServiceProvider<Potlatch> {
    private final String mBaseUrl;

    public PotlatchServiceProvider(
            final String baseUrl, final String clientId, final String clientSecret) {
        super(new PotlatchOAuth2Template(baseUrl, clientId, clientSecret));

        mBaseUrl = baseUrl;
    }

    @Override
    public Potlatch getApi(final String accessToken) {
        return new PotlatchTemplate(mBaseUrl, accessToken);
    }
}