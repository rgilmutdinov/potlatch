package com.zsoft.android.potlatch.ui.activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.zsoft.android.potlatch.DaggerInjectionUtils;
import com.zsoft.android.potlatch.R;
import com.zsoft.android.potlatch.ui.fragment.PaginatedGiftListFragment;
import com.zsoft.android.potlatch.ui.fragment.UserGiftListFragment;
import com.zsoft.android.potlatch.util.GiftOrder;

public class UserGiftListActivity extends NavDrawerGiftListActivity {
    protected static final String TAG = ChainedGiftListActivity.class.getSimpleName();
    private TextView mTxtTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerInjectionUtils.inject(this);
        mTxtTitle = (TextView) findViewById(R.id.title);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    protected int getActivityLayoutId() {
        return R.layout.activity_gift_list_with_title;
    }

    @Override
    protected PaginatedGiftListFragment createListFragment() {
        UserGiftListFragment giftListFragment = new UserGiftListFragment();
        giftListFragment.setOrder(GiftOrder.ORDER_NEWEST);
        return giftListFragment;
    }

    @Override
    protected int getToolbarIcon() {
        return R.drawable.ic_up;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch(id) {
            case R.id.action_settings:
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);

        mTxtTitle.setText(title);
    }
}
