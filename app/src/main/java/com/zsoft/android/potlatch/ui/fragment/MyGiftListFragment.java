package com.zsoft.android.potlatch.ui.fragment;

import com.zsoft.android.potlatch.util.GiftOrder;
import com.zsoft.android.potlatch.model.Gift;
import com.zsoft.android.potlatch.rest.api.GiftOperations;

import java.util.ArrayList;

public class MyGiftListFragment extends GiftListFragment {

    public MyGiftListFragment() {

    }

    @Override
    protected ArrayList<Gift> loadPage(String query, int pageId, int pageSize, int order) {
        GiftOperations giftOperations = mPotlatch.giftOperations();
        GiftOrder ordering = GiftOrder.getOrder(order);
        return new ArrayList<Gift>(giftOperations.searchMyGifts(query, pageId, pageSize,
                ordering.getOrderBy(), ordering.isAscendingOrder()));
    }

    @Override
    protected ArrayList<Gift> loadPage(int pageId, int pageSize, int order) {
        GiftOperations giftOperations = mPotlatch.giftOperations();
        GiftOrder ordering = GiftOrder.getOrder(order);
        return new ArrayList<Gift>(giftOperations.getMyGifts(pageId, pageSize,
                ordering.getOrderBy(), ordering.isAscendingOrder()));
    }
}
