package com.zsoft.android.potlatch.rest.api.impl;

import com.zsoft.android.potlatch.rest.RestTemplateUtils;

import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.social.oauth2.AbstractOAuth2ApiBinding;
import org.springframework.web.client.RestTemplate;

public class AbstractPotlatchOperations extends AbstractOAuth2ApiBinding {
    protected final String mBaseUrl;

    public AbstractPotlatchOperations(final String baseUrl, final String accessToken) {
        super(accessToken);

        mBaseUrl = baseUrl;
    }

    protected String buildUri(String path) {
        return mBaseUrl + path;
    }

    @Override
    protected FormHttpMessageConverter getFormMessageConverter() {
        FormHttpMessageConverter converter = super.getFormMessageConverter();
        converter.addPartConverter(new MappingJackson2HttpMessageConverter());
        return converter;
    }

    @Override
    protected void configureRestTemplate(RestTemplate restTemplate) {
        RestTemplateUtils.setTemplateUnsafe(restTemplate);
    }
}
