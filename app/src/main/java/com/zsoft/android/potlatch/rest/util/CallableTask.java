package com.zsoft.android.potlatch.rest.util;

import java.util.concurrent.Callable;

import android.os.AsyncTask;
import android.util.Log;


public class CallableTask<T> extends AsyncTask<Void,Double,T> {

    private static final String TAG = CallableTask.class.getName();

    public static <V> void invoke(Callable<V> call, TaskCallback<V> callback){
        new CallableTask<V>(call, callback).execute();
    }

    private Callable<T> mCallable;
    private TaskCallback<T> mCallback;
    private Exception mError;

    public CallableTask(Callable<T> callable, TaskCallback<T> callback) {
        mCallable = callable;
        mCallback = callback;
    }

    @Override
    protected T doInBackground(Void... ts) {
        T result = null;
        try {
            result = mCallable.call();
        } catch (Exception e){
            Log.e(TAG, "Error invoking callable in AsyncTask callable: "+ mCallable, e);
            mError = e;
        }
        return result;
    }

    @Override
    protected void onPostExecute(T r) {
        if (mError != null) {
            mCallback.error(mError);
        } else {
            mCallback.success(r);
        }
    }
}
