package com.zsoft.android.potlatch.ui.adapter;

import android.content.Context;

import com.zsoft.android.potlatch.model.Gift;

public abstract class AbstractGiftListAdapter extends AbstractListAdapter<Gift> {

    public AbstractGiftListAdapter(Context context) {
        super(context);
    }
}
