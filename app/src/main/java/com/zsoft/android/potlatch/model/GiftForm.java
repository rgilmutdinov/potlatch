package com.zsoft.android.potlatch.model;

import com.google.common.base.Objects;

public class GiftForm {
    private Long parentId;

    private String title;
    private String text;
    
    public GiftForm() {
        
    }

    public GiftForm(Long parentId, String title, String text) {
        this.parentId = parentId;
        this.title    = title;
        this.text     = text;
    }

    public Long getParentId() {
        return parentId;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof GiftForm) {
            GiftForm other = (GiftForm) o;
            return Objects.equal(parentId, other.parentId)
                    && Objects.equal(title, other.title)
                    && Objects.equal(text, other.text);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(parentId, title, text);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("parentId", parentId)
                .add("title", title)
                .add("text", text)
                .toString();
    }
}
