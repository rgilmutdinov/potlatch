package com.zsoft.android.potlatch.ui.adapter;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zsoft.android.potlatch.R;
import com.zsoft.android.potlatch.model.Gift;
import com.zsoft.android.potlatch.util.DateUtilsEx;

public class ChainedGiftListAdapter extends AbstractGiftListAdapter {
    protected static final String TAG = ChainedGiftListAdapter.class.getSimpleName();

    public static class ViewHolder {
        public TextView txtTitle;
        public TextView txtOwner;
        public TextView txtRating;
        public TextView txtViews;
        public TextView txtDate;

        public ImageView imgContent;

        public ViewHolder(View view) {
            this.txtTitle   = (TextView) view.findViewById(R.id.gift_title);
            this.txtOwner   = (TextView) view.findViewById(R.id.gift_owner);
            this.txtRating  = (TextView) view.findViewById(R.id.gift_rating);
            this.txtViews   = (TextView) view.findViewById(R.id.gift_views);
            this.txtDate    = (TextView) view.findViewById(R.id.gift_date);

            this.imgContent = (ImageView) view.findViewById(R.id.gift_content_image);
        }
    }

    public ChainedGiftListAdapter(Context context) {
        super(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.chained_gift_list_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Gift gift = getItem(position);
        if (gift != null) {
            holder.txtTitle.setText(gift.getTitle());
            holder.txtOwner.setText(gift.getOwnerName());
            holder.txtViews.setText(String.valueOf(gift.getViews()));
            holder.txtRating.setText(String.valueOf(gift.getRating()));

            long age = gift.getAge();
            if (age > 0) {
                int flags = DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_YEAR | DateUtils.FORMAT_ABBREV_MONTH;
                long created = gift.getCreated().getTimeInMillis();

                // use custom DateUtilsEx instead of system DateUtils
                // in order to display string in english locale only
                CharSequence relativeTime = DateUtilsEx
                        .getRelativeTimeSpanString(created, created + gift.getAge(),
                                DateUtils.SECOND_IN_MILLIS, flags, mContext);
                holder.txtDate.setVisibility(View.VISIBLE);
                holder.txtDate.setText(relativeTime);
            } else {
                holder.txtDate.setVisibility(View.INVISIBLE);
            }

            String url = gift.getUrl();
            if (url != null) {
                Picasso.with(mContext)
                        .load(url)
                        .placeholder(R.drawable.hp_photos_small)
                        .error(R.drawable.hp_caution_small)
                        .fit()
                        .centerInside()
                        .noFade()
                        .into(holder.imgContent);
                holder.imgContent.setContentDescription(gift.getTitle());
            } else {
                Picasso.with(mContext).cancelRequest(holder.imgContent);
                holder.imgContent.setImageDrawable(null);
                holder.imgContent.setContentDescription(null);
            }
        }

        return convertView;
    }
}
