package com.zsoft.android.potlatch.rest.connect;

import com.zsoft.android.potlatch.rest.api.Potlatch;

import org.springframework.social.connect.support.OAuth2ConnectionFactory;

public class PotlatchConnectionFactory extends OAuth2ConnectionFactory<Potlatch> {
    public PotlatchConnectionFactory(final String baseUrl, final String clientId, final String clientSecret) {
        super(Potlatch.PROVIDER_ID,
                new PotlatchServiceProvider(baseUrl, clientId, clientSecret),
                new PotlatchAdapter());
    }
}
