package com.zsoft.android.potlatch.util;

import android.content.Context;
import android.util.Log;

import com.zsoft.android.potlatch.model.User;

public class LoginAndAuthHelper {

    private static final String TAG = LoginAndAuthHelper.class.getSimpleName();

    private Context mAppContext;

    public LoginAndAuthHelper(Context mAppContext) {
        this.mAppContext = mAppContext;
    }

    public void onLogin(User currentUser) {

        AccountUtils.setActiveAccount(mAppContext, currentUser.getUsername());

        // Record profile ID, image URL and name
        Log.d(TAG, "Saving potlatch profile ID: " + currentUser.getId());

        AccountUtils.setProfileId(mAppContext, currentUser.getUsername(), currentUser.getId());
        AccountUtils.setImageUrl(mAppContext, currentUser.getUsername(), currentUser.getGravatarUrl());
        AccountUtils.setName(mAppContext, currentUser.getUsername(), currentUser.getUsername());
        AccountUtils.setEmail(mAppContext, currentUser.getUsername(), currentUser.getEmail());
    }

    public boolean logout() {
        AccountUtils.setActiveAccount(mAppContext, null);

        return true;
    }
}
