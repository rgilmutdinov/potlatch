package com.zsoft.android.potlatch.ui.fragment;

import android.accounts.Account;
import android.content.Intent;
import android.view.View;

import com.zsoft.android.potlatch.AppPreferences;
import com.zsoft.android.potlatch.Constants;
import com.zsoft.android.potlatch.R;
import com.zsoft.android.potlatch.model.Gift;
import com.zsoft.android.potlatch.rest.api.GiftOperations;
import com.zsoft.android.potlatch.ui.activity.ChainedGiftListActivity;
import com.zsoft.android.potlatch.ui.adapter.AbstractGiftListAdapter;
import com.zsoft.android.potlatch.ui.adapter.GiftListAdapter;
import com.zsoft.android.potlatch.util.AccountUtils;
import com.zsoft.android.potlatch.util.GiftOrder;

import java.util.ArrayList;

public class GiftListFragment extends PaginatedGiftListFragment implements View.OnClickListener {
    protected static final String TAG = GiftListFragment.class.getSimpleName();

    public GiftListFragment() {
    }

    @Override
    public void onClick(View v) {
        Object tag = v.getTag();
        if (tag != null) {
            final int position = Integer.parseInt(tag.toString());
            Gift gift = getAdapter().getItem(position);
            switch (v.getId()) {
                case R.id.gift_chain:
                    Intent intent = new Intent(getActivity(), ChainedGiftListActivity.class);
                    intent.putExtra(Constants.EXTRA_GIFT, gift);
                    startActivity(intent);
                    break;
                case R.id.gift_delete:
                    deleteGift(gift.getId());
                    break;
            }
        }
    }

    @Override
    protected AbstractGiftListAdapter createAdapter() {
        Account chosenAccount = AccountUtils.getActiveAccount(getActivity());
        Long userId = null;
        if (chosenAccount != null) {
            userId = AccountUtils.getProfileId(getActivity());
        }
        return new GiftListAdapter(getActivity(), this, userId);
    }

    @Override
    protected ArrayList<Gift> loadPage(String query, int pageId, int pageSize, int order) {
        boolean includeNegativeRated = !AppPreferences.hideObsceneGifts(getActivity());

        GiftOperations giftOperations = mPotlatch.giftOperations();
        GiftOrder ordering = GiftOrder.getOrder(order);
        return new ArrayList<Gift>(giftOperations.searchGifts(query, includeNegativeRated, pageId, pageSize,
                ordering.getOrderBy(), ordering.isAscendingOrder()));
    }

    @Override
    protected ArrayList<Gift> loadPage(int pageId, int pageSize, int order) {
        boolean includeNegativeRated = !AppPreferences.hideObsceneGifts(getActivity());

        GiftOperations giftOperations = mPotlatch.giftOperations();
        GiftOrder ordering = GiftOrder.getOrder(order);
        return new ArrayList<Gift>(giftOperations.getGifts(includeNegativeRated, pageId, pageSize,
                ordering.getOrderBy(), ordering.isAscendingOrder()));
    }
}
