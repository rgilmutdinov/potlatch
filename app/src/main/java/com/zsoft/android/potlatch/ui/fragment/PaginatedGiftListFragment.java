package com.zsoft.android.potlatch.ui.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.melnykov.fab.FloatingActionButton;
import com.zsoft.android.potlatch.AppPreferences;
import com.zsoft.android.potlatch.Constants;
import com.zsoft.android.potlatch.DaggerInjectionUtils;
import com.zsoft.android.potlatch.R;
import com.zsoft.android.potlatch.model.Gift;
import com.zsoft.android.potlatch.rest.api.GiftOperations;
import com.zsoft.android.potlatch.rest.api.Potlatch;
import com.zsoft.android.potlatch.rest.util.CallableTask;
import com.zsoft.android.potlatch.rest.util.TaskCallback;
import com.zsoft.android.potlatch.service.RefreshRequestService;
import com.zsoft.android.potlatch.ui.activity.CreateGiftActivity;
import com.zsoft.android.potlatch.ui.activity.ViewGiftActivity;
import com.zsoft.android.potlatch.ui.adapter.AbstractGiftListAdapter;
import com.zsoft.android.potlatch.ui.adapter.AbstractListAdapter;
import com.zsoft.android.potlatch.ui.adapter.SimpleEndlessAdapter;
import com.zsoft.android.potlatch.util.GiftOrder;

import org.springframework.social.connect.ConnectionRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

public abstract class PaginatedGiftListFragment extends SwipeRefreshListFragment
        implements SwipeRefreshLayout.OnRefreshListener {
    protected static final String TAG = PaginatedGiftListFragment.class.getSimpleName();

    protected static final int CODE_VIEW_GIFT = 1;
    protected static final int CODE_ADD_GIFT  = 2;

    @Inject
    ConnectionRepository mConnectionRepository;

    protected Potlatch mPotlatch;

    private int pageSize = 10;

    private AbstractGiftListAdapter mAdapter;
    private SimpleEndlessAdapter mEndlessAdapter;

    private ArrayList<Gift> mGifts;
    private String mSearchQuery = null;
    private int mOrder = GiftOrder.ORDER_NEWEST;

    private boolean mDestroyed = false;

    protected FloatingActionButton mFloatingActionButton;

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            refreshGifts();
        }
    };

    private ProgressDialog mProgressDialog;

    public PaginatedGiftListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerInjectionUtils.inject(this);

        mPotlatch = mConnectionRepository.findPrimaryConnection(Potlatch.class).getApi();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View listFragmentView = super.onCreateView(inflater, container, savedInstanceState);

        FrameLayout root = new FrameLayout(getActivity());
        root.addView(listFragmentView,
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        root.setLayoutParams(
                new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
        mFloatingActionButton = (FloatingActionButton) inflater.inflate(R.layout.floating_action_button, container, false);
        root.addView(mFloatingActionButton);

        setHasOptionsMenu(true);

        pageSize = AppPreferences.getItemsPerPage(getActivity());

        return root;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAdapter = createAdapter();
        mEndlessAdapter = new SimpleEndlessAdapter(getActivity(), mAdapter) {
            public void doOnLoading() {
                getNextListPage();
            }
        };
        setListAdapter(mEndlessAdapter);
        setEmptyText(getString(R.string.list_no_gifts));
        getListView().setDivider(null);

        int height = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 5,
                getActivity().getResources().getDisplayMetrics()
        );
        getListView().setDividerHeight(height);

        if (mGifts != null) {
            setNewList(mGifts, false);
        } else {
            getNewList();
        }

        setListeners();
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getApplicationContext().registerReceiver(
                receiver, new IntentFilter(RefreshRequestService.REFRESH_NOTIFICATION));
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().getApplicationContext()
                .unregisterReceiver(receiver);
    }

    protected AbstractListAdapter<Gift> getAdapter() {
        return mAdapter;
    }

    protected abstract AbstractGiftListAdapter createAdapter();
    protected abstract ArrayList<Gift> loadPage(String query, int pageId, int pageSize, int order);
    protected abstract ArrayList<Gift> loadPage(int pageId, int pageSize, int order);
    protected void configureGiftCreateIntent(final Intent intent) {

    }

    protected ArrayList<Gift> refreshGifts(List<Long> giftsIds) {
        GiftOperations giftOperations = mPotlatch.giftOperations();
        List<Gift> refreshedGifts = giftOperations.refreshGifts(giftsIds);
        return new ArrayList<Gift>(refreshedGifts);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        inflater.inflate(R.menu.menu_gift_list, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = new SearchView(((ActionBarActivity) getActivity())
                .getSupportActionBar().getThemedContext());
        MenuItemCompat.setShowAsAction(item,
                MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);

        MenuItemCompat.setActionView(item, searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Called when the action bar search text has changed. Update
                // the search filter, and reload gifts.
                mSearchQuery = query;
                getNewList();

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // Don't care about this.
                return true;
            }
        });

        MenuItemCompat.setOnActionExpandListener(item, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                mSearchQuery = null;
                getNewList();

                return true;
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }
        });
    }

    private void setListeners() {
        mFloatingActionButton.attachToListView(getListView(), new FloatingActionButton.FabOnScrollListener() {
            @Override
            public void onScrollDown() {
                mFloatingActionButton.show(true);
            }

            @Override
            public void onScrollUp() {
                mFloatingActionButton.hide(true);
            }
        });

        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CreateGiftActivity.class);
                configureGiftCreateIntent(intent);
                startActivityForResult(intent, CODE_ADD_GIFT);
            }
        });

        setOnRefreshListener(this);

        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), ViewGiftActivity.class);
                intent.putExtra(Constants.EXTRA_GIFT, (Gift) adapterView.getItemAtPosition(position));
                startActivityForResult(intent, CODE_VIEW_GIFT);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch(id) {
            case R.id.action_refresh:
                getNewList();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case CODE_VIEW_GIFT:
                    try {
                        Gift resultGift = (Gift) data.getSerializableExtra(Constants.EXTRA_GIFT);
                        if (mGifts != null) {
                            int index = mGifts.indexOf(resultGift);
                            if (index >= 0) {
                                mGifts.set(index, resultGift);
                                mAdapter.notifyDataSetChanged();
                            }
                        }
                    } catch (Exception exc) {
                        Log.w(TAG, "Could not update gift info.", exc);
                    }
                    break;
                case CODE_ADD_GIFT:
                    // reload list
                    getNewList();
                    break;
            }
        }
    }

    @Override
    public void onRefresh() {
        getNewList();
    }

    protected void getNewList() {
        getNewList(false);
    }

    private void getNextListPage() {
        getNewList(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mAdapter = null;
        mEndlessAdapter = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mGifts = null;
        mDestroyed = true;
    }

    private void refreshGifts() {
        try {
            final List<Long> ids = new ArrayList<Long>();
            for (Gift gift : mGifts) {
                ids.add(gift.getId());
            }

            CallableTask.invoke(
                    new Callable<ArrayList<Gift>>() {
                        @Override
                        public ArrayList<Gift> call() throws Exception {
                           return refreshGifts(ids);
                        }
                    },
                    new TaskCallback<ArrayList<Gift>>() {
                        @Override
                        public void success(ArrayList<Gift> gifts) {
                            if (getActivity() != null && !mDestroyed) {
                                refreshAdapter(gifts);
                            }
                        }

                        @Override
                        public void error(Exception exc) {
                            Log.w("Unable to refresh gifts.", exc);
                        }
                    });
        } catch (Exception exc) {
            Log.w("Unable to refresh gifts.", exc);
        }
    }

    private void refreshAdapter(ArrayList<Gift> gifts) {
        if (mGifts != null) {
            for (Gift refreshedGift : gifts) {
                int index = mGifts.indexOf(refreshedGift);
                if (index >= 0) {
                    mGifts.set(index, refreshedGift);
                }
            }
            mAdapter.notifyDataSetChanged();
        }
    }

    protected void setNewList(ArrayList<Gift> newList, boolean appendResults) {
        if (appendResults) {
            if (newList == null) {
                this.mGifts = null;
            } else {
                this.mGifts.addAll(newList);
            }
        } else {
            this.mGifts = newList;
        }

        if (getView() == null) {
            return;
        }

        setRefreshing(false);
        setListShown(true);

        mAdapter.setItems(mGifts);
        mEndlessAdapter.onDataReady();
        if (newList != null && newList.size() >= pageSize) {
            mEndlessAdapter.restartAppending();
        } else {
            mEndlessAdapter.stopAppending();
        }

        if (!appendResults && !mEndlessAdapter.isEmpty()) {
            getListView().setSelection(0);	// reset list scroll position after setting new list
        }
    }

    private void getNewList(final boolean isNextPage) {
        if (!isNextPage) {
            mGifts = null;
        }

        try {
            final int pageId = (mGifts != null ? mGifts.size() / pageSize : 0);

            CallableTask.invoke(
                    new Callable<ArrayList<Gift>>() {
                        @Override
                        public ArrayList<Gift> call() throws Exception {
                            if (!TextUtils.isEmpty(mSearchQuery)) {
                                return loadPage(mSearchQuery, pageId, pageSize, mOrder);
                            }
                            return loadPage(pageId, pageSize, mOrder);
                        }
                    },
                    new TaskCallback<ArrayList<Gift>>() {
                        @Override
                        public void success(ArrayList<Gift> gifts) {
                            if (getActivity() != null && !mDestroyed) {
                                setNewList(gifts, isNextPage);
                            }
                        }

                        @Override
                        public void error(Exception e) {
                            if (getActivity() != null && !mDestroyed) {
                                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
                                setNewList(null, isNextPage);
                            }
                        }
                    });

            if (!isNextPage && getListView() != null) {
                // hide ListView and show progress bar only when loading the first page
                setListShownNoAnimation(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
            setNewList(null, isNextPage);
        }
    }

    public void deleteGift(final Long giftId) {
        Activity context = getActivity();
        new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.dialog_remove_gift_title))
                .setIcon(android.R.drawable.ic_menu_delete)
                .setMessage(context.getString(R.string.dialog_remove_gift_body))
                .setPositiveButton(context.getString(R.string.dialog_remove_gift_ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        showProgressDialog(getActivity().getString(R.string.dialog_deleting_gift));
                        CallableTask.invoke(
                                new Callable<Boolean>() {
                                    @Override
                                    public Boolean call() throws Exception {
                                        mPotlatch.giftOperations().deleteGift(giftId);
                                        return true;
                                    }
                                },
                                new TaskCallback<Boolean>() {
                                    @Override
                                    public void success(Boolean result) {
                                        if (getActivity() != null && !mDestroyed) {
                                            dismissProgressDialog();
                                            getNewList();
                                        }
                                    }

                                    @Override
                                    public void error(Exception e) {
                                        if (getActivity() != null && !mDestroyed) {
                                            dismissProgressDialog();
                                            Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                    }
                })
                .setNegativeButton(context.getString(R.string.dialog_remove_selected_gift_cancel), null)
                .show();
    }

    public void setOrder(int order) {
        mOrder = order;
    }

    public void reloadGifts() {
        getNewList();
    }

    public void showProgressDialog(CharSequence message) {
        if (this.mProgressDialog == null) {
            this.mProgressDialog = new ProgressDialog(getActivity());
            this.mProgressDialog.setIndeterminate(true);
        }

        this.mProgressDialog.setMessage(message);
        this.mProgressDialog.setCancelable(false);
        this.mProgressDialog.show();
    }

    public void dismissProgressDialog() {
        if (this.mProgressDialog != null && !this.mDestroyed) {
            this.mProgressDialog.dismiss();
        }
    }
}
