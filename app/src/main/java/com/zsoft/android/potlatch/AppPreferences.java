package com.zsoft.android.potlatch;

import android.content.Context;
import android.preference.PreferenceManager;

public class AppPreferences {

    public static int getItemsPerPage(Context context) {
        String key = context.getString(R.string.key_items_per_page);
        String value = PreferenceManager.getDefaultSharedPreferences(context).getString(key, "10");
        return Integer.valueOf(value);
    }

    public static boolean hideObsceneGifts(Context context) {
        String key = context.getString(R.string.key_hide_obscene_gifts);
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(key, false);
    }

    public static boolean autoRefreshEnabled(Context context) {
        String key = context.getString(R.string.key_auto_refresh_enabled);
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(key, false);
    }

    public static int refreshInterval(Context context) {
        String key = context.getString(R.string.key_refresh_interval);
        String value = PreferenceManager.getDefaultSharedPreferences(context).getString(key, "5");
        return Integer.valueOf(value);
    }
}
