package com.zsoft.android.potlatch.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.zsoft.android.potlatch.Constants;
import com.zsoft.android.potlatch.DaggerInjectionUtils;
import com.zsoft.android.potlatch.R;
import com.zsoft.android.potlatch.model.Gift;
import com.zsoft.android.potlatch.rest.api.GiftOperations;
import com.zsoft.android.potlatch.rest.api.Potlatch;
import com.zsoft.android.potlatch.rest.util.CallableTask;
import com.zsoft.android.potlatch.rest.util.TaskCallback;

import org.springframework.social.connect.ConnectionRepository;

import java.text.SimpleDateFormat;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import uk.co.senab.photoview.PhotoView;

public class ViewGiftActivity extends AbstractActionBarDialogActivity implements View.OnClickListener {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private static final int VOTE_UP     = 1;
    private static final int VOTE_ABSENT = 0;
    private static final int VOTE_DOWN   = -1;

    @Inject
    ConnectionRepository mConnectionRepository;

    private Potlatch mPotlatch;

    private Gift mCurrentGift;

    private View mLoadingLayout;
    private View mGiftIntoLayout;

    private PhotoView mImgContent;

    private ImageView mImgVoteUp;
    private ImageView mImgVoteDown;
    private ImageView mImgChain;
    private TextView mTxtRating;

    private TextView mTxtViews;
    private TextView mTxtDate;
    private TextView mTxtOwner;
    private TextView mTxtText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerInjectionUtils.inject(this);
        setContentView(R.layout.activity_view_gift);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationIcon(R.drawable.ic_up);
        }

        mLoadingLayout = findViewById(R.id.loading_layout);
        mGiftIntoLayout = findViewById(R.id.gift_info_layout);

        mImgContent  = (PhotoView) findViewById(R.id.image);
        mImgVoteUp   = (ImageView) findViewById(R.id.gift_vote_up);
        mImgVoteDown = (ImageView) findViewById(R.id.gift_vote_down);
        mImgChain    = (ImageView) findViewById(R.id.gift_chain);

        mTxtRating   = (TextView) findViewById(R.id.gift_rating);
        mTxtViews    = (TextView) findViewById(R.id.gift_views);
        mTxtDate     = (TextView) findViewById(R.id.gift_date);
        mTxtOwner    = (TextView) findViewById(R.id.gift_owner);
        mTxtText     = (TextView) findViewById(R.id.gift_text);

        Bundle bundle = getIntent().getExtras();
        mCurrentGift = (Gift) bundle.getSerializable(Constants.EXTRA_GIFT);

        mPotlatch = mConnectionRepository.findPrimaryConnection(Potlatch.class).getApi();

        mLoadingLayout.setVisibility(View.VISIBLE);
        mGiftIntoLayout.setVisibility(View.GONE);

        mImgVoteUp.setOnClickListener(this);
        mImgVoteDown.setOnClickListener(this);

        fillViews();
        loadGiftInfo();
    }

    private void fillViews() {
        if (mCurrentGift != null) {
            if (mCurrentGift.getParentId() == null) {
                mImgChain.setVisibility(View.VISIBLE);
                mImgChain.setOnClickListener(this);
            }
            setTitle(mCurrentGift.getTitle());
            String url = mCurrentGift.getUrl();

            if (url != null) {
                Picasso.with(this)
                        .load(url)
                        .placeholder(R.drawable.hp_photos_small)
                        .error(R.drawable.hp_caution_small)
                        //.fit()
                        //.centerCrop()
                        .noFade()
                        .into(mImgContent, new Callback() {
                            @Override
                            public void onSuccess() {
                                mImgContent.setScaleType(ImageView.ScaleType.CENTER_CROP);

                            }

                            @Override
                            public void onError() {

                            }
                        });
            } else {
                Picasso.with(this).cancelRequest(mImgContent);
                mImgContent.setImageDrawable(null);
                mImgContent.setContentDescription(null);
            }

            mTxtRating.setText(String.valueOf(mCurrentGift.getRating()));
            mTxtViews.setText(String.valueOf(mCurrentGift.getViews()));

            mTxtDate.setText(DATE_FORMAT.format(mCurrentGift.getCreated().getTime()));
            mTxtOwner.setText(mCurrentGift.getOwnerName());

            String text = mCurrentGift.getText();
            if (text != null) {
                mTxtText.setText(text);
            }
        }
    }

    protected void loadGiftInfo() {
        CallableTask.invoke(
                new Callable<Gift>() {
                    @Override
                    public Gift call() throws Exception {
                        GiftOperations operations = mPotlatch.giftOperations();
                        return operations.getGift(mCurrentGift.getId());
                    }
                },
                new TaskCallback<Gift>() {
                    @Override
                    public void success(Gift gift) {
                        if (!mDestroyed) {
                            mLoadingLayout.setVisibility(View.GONE);
                            mGiftIntoLayout.setVisibility(View.VISIBLE);

                            mCurrentGift = gift;

                            updateVoteIcons(mCurrentGift.getVote());

                            mTxtRating.setText(String.valueOf(gift.getRating()));
                            mTxtViews.setText(String.valueOf(gift.getViews()));
                        }
                    }

                    @Override
                    public void error(Exception e) {
                        if (!mDestroyed) {
                            mLoadingLayout.setVisibility(View.GONE);
                            mGiftIntoLayout.setVisibility(View.VISIBLE);

                            showMessage(getString(R.string.error_load_gift_info),
                                    e.getLocalizedMessage());
                        }
                    }
                }
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_gift, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(Constants.EXTRA_GIFT, mCurrentGift);

        setResult(RESULT_OK, intent);
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_settings:
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.gift_vote_up:
                makeVote(VOTE_UP);
                break;
            case R.id.gift_vote_down:
                makeVote(VOTE_DOWN);
                break;
            case R.id.gift_chain:
                if (mCurrentGift != null) {
                    Intent intent = new Intent(this, ChainedGiftListActivity.class);
                    intent.putExtra(Constants.EXTRA_GIFT, mCurrentGift);
                    startActivity(intent);
                }
                break;
        }
    }

    private void updateVoteIcons(int vote) {
        int colorNone = getResources().getColor(R.color.primary_dark_material_light);
        mImgVoteDown.setColorFilter(colorNone);
        mImgVoteUp.setColorFilter(colorNone);
        switch (vote) {
            case VOTE_UP:
                mImgVoteUp.setColorFilter(getResources().getColor(R.color.colorAccent));
                break;
            case VOTE_DOWN:
                mImgVoteDown.setColorFilter(getResources().getColor(R.color.colorAccent));
                break;
        }
    }

    private void makeVote(final int vote) {
        mImgVoteUp.setEnabled(false);
        mImgVoteDown.setEnabled(false);

        CallableTask.invoke(
                new Callable<Gift>() {
                    @Override
                    public Gift call() throws Exception {
                        GiftOperations operations = mPotlatch.giftOperations();

                        switch (vote) {
                            case VOTE_UP:
                                return operations.giftVoteUp(mCurrentGift.getId());
                            default:
                                return operations.giftVoteDown(mCurrentGift.getId());
                        }
                    }
                },
                new TaskCallback<Gift>() {
                    @Override
                    public void success(Gift gift) {
                        if (!mDestroyed) {
                            mCurrentGift = gift;

                            updateVoteIcons(mCurrentGift.getVote());

                            mTxtRating.setText(String.valueOf(gift.getRating()));
                            mImgVoteUp.setEnabled(true);
                            mImgVoteDown.setEnabled(true);
                        }
                    }

                    @Override
                    public void error(Exception e) {
                        if (!mDestroyed) {
                            mImgVoteUp.setEnabled(true);
                            mImgVoteDown.setEnabled(true);

                            showMessage(getString(R.string.error_vote_gift),
                                    e.getLocalizedMessage());
                        }
                    }
                }
        );
    }
}
