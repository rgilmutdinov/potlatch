package com.zsoft.android.potlatch.ui.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.ActionBarActivity;

public class AbstractActionBarDialogActivity extends ActionBarActivity {
    protected static final String TAG = AbstractActionBarDialogActivity.class.getSimpleName();

    private ProgressDialog mProgressDialog;

    protected boolean mDestroyed = false;

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mDestroyed = true;
    }

    public void showProgressDialog(CharSequence message) {
        if (this.mProgressDialog == null) {
            this.mProgressDialog = new ProgressDialog(this);
            this.mProgressDialog.setIndeterminate(true);
        }

        this.mProgressDialog.setMessage(message);
        this.mProgressDialog.setCancelable(false);
        this.mProgressDialog.show();
    }

    public void dismissProgressDialog() {
        if (this.mProgressDialog != null && !this.mDestroyed) {
            this.mProgressDialog.dismiss();
        }
    }

    protected void showMessage(String title, String message) {
        showMessage(title, message, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

    protected void showMessage(String title, String message, DialogInterface.OnClickListener onOkClickListener) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(title);
        alertDialog.setCancelable(true);
        alertDialog.setPositiveButton("OK", onOkClickListener);
        alertDialog.setMessage(message);
        alertDialog.show();
    }
}
