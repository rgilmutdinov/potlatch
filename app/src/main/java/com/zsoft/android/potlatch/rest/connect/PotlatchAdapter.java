package com.zsoft.android.potlatch.rest.connect;

import com.zsoft.android.potlatch.rest.api.Potlatch;
import com.zsoft.android.potlatch.model.User;

import org.springframework.social.ApiException;
import org.springframework.social.connect.ApiAdapter;
import org.springframework.social.connect.ConnectionValues;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.connect.UserProfileBuilder;

public class PotlatchAdapter implements ApiAdapter<Potlatch> {

    @Override
    public boolean test(final Potlatch api) {
        try {
            api.userOperations().getProfile();
            return true;
        } catch (ApiException e) {
            return false;
        }
    }

    @Override
    public void setConnectionValues(final Potlatch api, final ConnectionValues values) {

    }


    @Override
    public UserProfile fetchUserProfile(final Potlatch api) {
        User user = api.userOperations().getProfile();
        return new UserProfileBuilder().setName(user.getUsername()).setUsername(user.getUsername()).build();
    }

    @Override
    public void updateStatus(final Potlatch api, final String message) {
        // TODO Auto-generated method stub

    }

}
