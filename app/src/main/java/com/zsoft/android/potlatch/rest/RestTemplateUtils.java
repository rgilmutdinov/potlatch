package com.zsoft.android.potlatch.rest;

import android.util.Log;

import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.InterceptingClientHttpRequestFactory;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.social.support.ClientHttpRequestFactorySelector;
import org.springframework.social.support.FormMapHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.net.ssl.SSLContext;

public class RestTemplateUtils {
    protected static final String TAG = RestTemplateUtils.class.getSimpleName();

    public static RestTemplate createUnsafeTemplate() {
        RestTemplate restTemplate = createTemplateBase();

        setTemplateUnsafe(restTemplate);

        return restTemplate;
    }

    public static RestTemplate createUnsafeTemplate(ClientHttpRequestInterceptor... interceptors) {
        RestTemplate restTemplate = createTemplateBase();

        try {
            CloseableHttpClient httpClient = createUnsafeHttpClient();

            ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(
                    httpClient);
            restTemplate.setRequestFactory(requestFactory);

            List<ClientHttpRequestInterceptor> ris = new ArrayList<ClientHttpRequestInterceptor>();
            Collections.addAll(ris, interceptors);

            restTemplate.setInterceptors(ris);
            restTemplate.setRequestFactory(new InterceptingClientHttpRequestFactory(
                    requestFactory , ris));

        } catch (Exception exc) {
            Log.e(TAG, "Error instantiating rest template: " + exc.toString());
        }

        return restTemplate;
    }

    public static void setTemplateUnsafe(RestTemplate restTemplate) {
        try {
            CloseableHttpClient httpClient = createUnsafeHttpClient();

            ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(
                    httpClient);
            restTemplate.setRequestFactory(requestFactory);

        } catch (Exception exc) {
            Log.e(TAG, "Error instantiating rest template: " + exc.toString());
        }
    }

    private static RestTemplate createTemplateBase() {
        RestTemplate restTemplate = new RestTemplate(
                ClientHttpRequestFactorySelector.getRequestFactory());

        List<HttpMessageConverter<?>> converters = new ArrayList<HttpMessageConverter<?>>(2);

        converters.add(new FormHttpMessageConverter());
        converters.add(new FormMapHttpMessageConverter());
        converters.add(new MappingJackson2HttpMessageConverter());

        restTemplate.setMessageConverters(converters);

        return restTemplate;
    }

    private static CloseableHttpClient createUnsafeHttpClient()
            throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null,
                new TrustSelfSignedStrategy()).build();

        return HttpClients
                .custom()
                .setSslcontext(sslContext)
                .setHostnameVerifier(new AllowAllHostnameVerifier()) // allow all hostnames
                .build();
    }
}
