package com.zsoft.android.potlatch.service;

import android.app.IntentService;
import android.content.Intent;

public class RefreshRequestService extends IntentService {
    private static final String TAG = AlarmReceiver.class.getSimpleName();

    public static final String REFRESH_NOTIFICATION = "refresh_notification";

    public RefreshRequestService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        sendRefreshRequest();
    }

    private void sendRefreshRequest() {
        Intent intent = new Intent(REFRESH_NOTIFICATION);
        sendBroadcast(intent);
    }
} 