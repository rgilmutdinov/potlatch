package com.zsoft.android.potlatch.util;

public class GiftOrder implements Order {
    public static final int ORDER_NEWEST       = 1;
    public static final int ORDER_MOST_VIEWED  = 2;
    public static final int ORDER_TOP_RATED    = 3;
    public static final int ORDER_TITLE        = 4;
    public static final int ORDER_TITLE_DESC   = 5;

    public static final String FIELD_ID     = "id";
    public static final String FIELD_VIEWS  = "views";
    public static final String FIELD_RATING = "rating";
    public static final String FIELD_TITLE  = "title";

    private final String mOrderBy;
    private final boolean mAscendingOrder;

    private GiftOrder(String orderBy, boolean ascendingOrder) {
        mOrderBy = orderBy;
        mAscendingOrder = ascendingOrder;
    }

    public static GiftOrder getOrder(int order) {
        switch (order) {
            case ORDER_TOP_RATED:
                return new GiftOrder(FIELD_RATING, false);
            case ORDER_MOST_VIEWED:
                return new GiftOrder(FIELD_VIEWS, false);
            case ORDER_TITLE:
                return new GiftOrder(FIELD_TITLE, true);
            case ORDER_TITLE_DESC:
                return new GiftOrder(FIELD_TITLE, false);
            default:
                return new GiftOrder(FIELD_ID, false);
        }
    }

    @Override
    public String getOrderBy() {
        return mOrderBy;
    }

    @Override
    public boolean isAscendingOrder() {
        return mAscendingOrder;
    }
}
