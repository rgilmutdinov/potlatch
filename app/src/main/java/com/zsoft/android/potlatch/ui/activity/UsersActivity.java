package com.zsoft.android.potlatch.ui.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.zsoft.android.potlatch.R;
import com.zsoft.android.potlatch.ui.adapter.OrderSpinnerAdapter;
import com.zsoft.android.potlatch.ui.fragment.UserListFragment;
import com.zsoft.android.potlatch.util.OrderItem;
import com.zsoft.android.potlatch.util.UserOrder;

import java.util.ArrayList;

public class UsersActivity extends NavDrawerActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationIcon(R.drawable.ic_drawer);
        }

        if (savedInstanceState == null) {
            UserListFragment listFragment = new UserListFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, listFragment)
                    .commit();
        }

        View spinnerContainer = LayoutInflater.from(this).inflate(R.layout.actionbar_spinner,
                toolbar, false);
        ActionBar.LayoutParams lp = new ActionBar.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        toolbar.addView(spinnerContainer, lp);

        Spinner spinner = (Spinner) spinnerContainer.findViewById(R.id.actionbar_spinner);
        OrderSpinnerAdapter adapter = new OrderSpinnerAdapter(this, getOrderings());
        spinner.setAdapter(adapter);
        spinner.setSelection(0);
        spinner.setTag(0);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> spinner, View view, int position, long itemId) {
                UserListFragment userListFragment = (UserListFragment)
                        getSupportFragmentManager().findFragmentById(R.id.container);
                if (userListFragment != null) {
                    if (((Integer) spinner.getTag()) == position) {
                        return;
                    }
                    spinner.setTag(position);
                    OrderItem orderItem = (OrderItem) spinner.getItemAtPosition(position);
                    userListFragment.setOrder(orderItem.getKey());
                    userListFragment.reloadUsers();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private ArrayList<OrderItem> getOrderings() {
        ArrayList<OrderItem> orderItems = new ArrayList<OrderItem>();

        orderItems.add(new OrderItem(getString(R.string.user_order_top_givers),
                UserOrder.ORDER_TOP_GIVERS));
        orderItems.add(new OrderItem(getString(R.string.user_order_top_uploaders),
                UserOrder.ORDER_TOP_UPLOADERS));
        orderItems.add(new OrderItem(getString(R.string.user_order_by_username),
                UserOrder.ORDER_NAME));
        orderItems.add(new OrderItem(getString(R.string.user_order_by_username_desc),
                UserOrder.ORDER_NAME_DESC));

        return orderItems;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_users, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected int getSelfNavDrawerItem() {
        return NAVDRAWER_ITEM_USERS;
    }
}
