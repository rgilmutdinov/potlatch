package com.zsoft.android.potlatch.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zsoft.android.potlatch.R;
import com.zsoft.android.potlatch.model.User;

public class UserListAdapter extends AbstractListAdapter<User> {
    protected static final String TAG = UserListAdapter.class.getSimpleName();

    public static class ViewHolder {
        public ImageView imgUserImage;
        public TextView txtUsername;
        public TextView txtUserRating;
        public TextView txtUserGiftsPosted;

        public ViewHolder(View view) {
            this.imgUserImage = (ImageView) view.findViewById(R.id.imgUserImage);

            this.txtUsername  = (TextView) view.findViewById(R.id.txtUsername);
            this.txtUserRating = (TextView) view.findViewById(R.id.txtUserRating);
            this.txtUserGiftsPosted = (TextView) view.findViewById(R.id.txtUserGiftsPosted);
        }
    }

    public UserListAdapter(Context context) {
        super(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.user_list_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final User user = getItem(position);
        if (user != null) {
            holder.txtUsername.setText(user.getUsername());

            if (user.getRating() != null) {
                holder.txtUserRating.setText(Long.toString(user.getRating()));
            }

            if (user.getGiftsPosted() != null) {
                holder.txtUserGiftsPosted.setText(Long.toString(user.getGiftsPosted()));
            }

            String url = user.getGravatarUrl();
            if (url != null) {
                url += "?r=g&d=identicon";
                Picasso.with(mContext)
                        .load(url)
                        .error(R.drawable.account)
                        .fit()
                        .centerInside()
                        .noFade()
                        .into(holder.imgUserImage);
                holder.imgUserImage.setContentDescription(user.getUsername());
            } else {
                Picasso.with(mContext).cancelRequest(holder.imgUserImage);
                holder.imgUserImage.setImageDrawable(null);
                holder.imgUserImage.setContentDescription(null);
            }
        }

        return convertView;
    }
}
