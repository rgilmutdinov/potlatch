package com.zsoft.android.potlatch.util;

public class UserOrder implements Order {
    public static final int ORDER_TOP_GIVERS    = 1;
    public static final int ORDER_TOP_UPLOADERS = 2;
    public static final int ORDER_NAME          = 3;
    public static final int ORDER_NAME_DESC     = 4;

    public static final String FIELD_RATING       = "rating";
    public static final String FIELD_GIFTS_POSTED = "giftsPosted";
    public static final String FIELD_NAME         = "username";

    private final String mOrderBy;
    private final boolean mAscendingOrder;

    private UserOrder(String orderBy, boolean ascendingOrder) {
        mOrderBy = orderBy;
        mAscendingOrder = ascendingOrder;
    }

    public static UserOrder getOrder(int order) {
        switch (order) {
            case ORDER_TOP_GIVERS:
                return new UserOrder(FIELD_RATING, false);
            case ORDER_TOP_UPLOADERS:
                return new UserOrder(FIELD_GIFTS_POSTED, false);
            case ORDER_NAME:
                return new UserOrder(FIELD_NAME, true);
            default: // ORDER_NAME_DESC
                return new UserOrder(FIELD_NAME, false);
        }
    }

    @Override
    public String getOrderBy() {
        return mOrderBy;
    }

    @Override
    public boolean isAscendingOrder() {
        return mAscendingOrder;
    }
}
