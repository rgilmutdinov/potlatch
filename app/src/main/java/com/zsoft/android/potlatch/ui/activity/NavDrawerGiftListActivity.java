package com.zsoft.android.potlatch.ui.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.zsoft.android.potlatch.R;
import com.zsoft.android.potlatch.ui.adapter.OrderSpinnerAdapter;
import com.zsoft.android.potlatch.ui.fragment.PaginatedGiftListFragment;
import com.zsoft.android.potlatch.util.GiftOrder;
import com.zsoft.android.potlatch.util.OrderItem;

import java.util.ArrayList;

public abstract class NavDrawerGiftListActivity extends NavDrawerActivity {
    protected static final String TAG = NavDrawerGiftListActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getActivityLayoutId());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationIcon(getToolbarIcon());
        }

        if (savedInstanceState == null) {
            PaginatedGiftListFragment listFragment = createListFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, listFragment)
                    .commit();
        }

        View spinnerContainer = LayoutInflater.from(this).inflate(R.layout.actionbar_spinner,
                toolbar, false);
        ActionBar.LayoutParams lp = new ActionBar.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        toolbar.addView(spinnerContainer, lp);

        Spinner spinner = (Spinner) spinnerContainer.findViewById(R.id.actionbar_spinner);
        OrderSpinnerAdapter adapter = new OrderSpinnerAdapter(this, getOrderings());
        spinner.setAdapter(adapter);
        spinner.setSelection(0);
        spinner.setTag(0);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> spinner, View view, int position, long itemId) {
                PaginatedGiftListFragment giftListFragment = (PaginatedGiftListFragment)
                        getSupportFragmentManager().findFragmentById(R.id.container);
                if (giftListFragment != null) {
                    if (((Integer) spinner.getTag()) == position) {
                        return;
                    }
                    spinner.setTag(position);
                    OrderItem orderItem = (OrderItem) spinner.getItemAtPosition(position);
                    giftListFragment.setOrder(orderItem.getKey());
                    giftListFragment.reloadGifts();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    protected abstract int getActivityLayoutId();
    protected abstract PaginatedGiftListFragment createListFragment();
    protected abstract int getToolbarIcon();

    private ArrayList<OrderItem> getOrderings() {
        ArrayList<OrderItem> orderItems = new ArrayList<OrderItem>();

        orderItems.add(new OrderItem(getString(R.string.order_newest),      GiftOrder.ORDER_NEWEST));
        orderItems.add(new OrderItem(getString(R.string.order_most_viewed), GiftOrder.ORDER_MOST_VIEWED));
        orderItems.add(new OrderItem(getString(R.string.order_top_rated),   GiftOrder.ORDER_TOP_RATED));
        orderItems.add(new OrderItem(getString(R.string.order_title),       GiftOrder.ORDER_TITLE));
        orderItems.add(new OrderItem(getString(R.string.order_title_desc),  GiftOrder.ORDER_TITLE_DESC));

        return orderItems;
    }
}
