package com.zsoft.android.potlatch.model;

import com.google.common.base.Objects;

public class UserForm {
    private String username;
    private String email;
    private String password;
    
    public UserForm() {
        
    }
    
    public UserForm(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }
    
    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof UserForm) {
            UserForm other = (UserForm) o;
            return Objects.equal(username, other.username)
                    && Objects.equal(email, other.email)
                    && Objects.equal(password, other.password);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(username, email, password);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("username", username)
                .add("email", email)
                .add("password", password)
                .toString();
    }
}
