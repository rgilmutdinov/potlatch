package com.zsoft.android.potlatch.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.zsoft.android.potlatch.AppPreferences;

import java.util.Calendar;


public class AlarmReceiver extends BroadcastReceiver {
    private static final String TAG = AlarmReceiver.class.getSimpleName();

    public static final String REFRESH = "refresh";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getBooleanExtra(REFRESH, false)) {

            Intent intentService = new Intent(context, RefreshRequestService.class);
            context.startService(intentService);
        }

        start(context);
    }

    public static void start(Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent pendingIntent = createPendingIntent(context);

        long updateInterval = 60 * 1000 * AppPreferences.refreshInterval(context);

        Calendar currentCalendar = Calendar.getInstance();
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
                currentCalendar.getTimeInMillis() + updateInterval, updateInterval, pendingIntent);
    }

    private static PendingIntent createPendingIntent(Context context) {
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra(REFRESH, true);
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public static void stop(Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent pendingIntent = createPendingIntent(context);
        alarmManager.cancel(pendingIntent);
        pendingIntent.cancel();
    }

    public static void restart(Context context) {
        stop(context);
        start(context);
    }
}
