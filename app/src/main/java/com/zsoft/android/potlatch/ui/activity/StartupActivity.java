package com.zsoft.android.potlatch.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.zsoft.android.potlatch.DaggerInjectionUtils;
import com.zsoft.android.potlatch.PotlatchApplication;
import com.zsoft.android.potlatch.rest.api.Potlatch;
import com.zsoft.android.potlatch.rest.connect.PotlatchConnectionFactory;

import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionRepository;

import javax.inject.Inject;

public class StartupActivity extends Activity {
    protected static final String TAG = StartupActivity.class.getSimpleName();

    @Inject
    ConnectionRepository mConnectionRepository;

    @Inject
    PotlatchConnectionFactory mConnectionFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerInjectionUtils.inject(this);

        boolean needConnection = true;
        if (isConnected()) {
            try {
                Connection<Potlatch> connection =
                        mConnectionRepository.getPrimaryConnection(Potlatch.class);
                connection.getApi().restOperations();

                if (!connection.hasExpired()) {
                    needConnection = false;
                }
            } catch (Exception exc) {
                Log.d(TAG, "Connection failure: " + exc.toString());
            }
        }

        if (needConnection) {
            startConnectionActivity();
        } else {
            startMainActivity();
        }
    }

    private void startConnectionActivity() {
        startActivity(new Intent(this, ConnectionActivity.class));
        finish();
    }

    private void startMainActivity() {
        startActivity(new Intent(this, MainGiftListActivity.class));
        finish();
    }

    @Override
    public PotlatchApplication getApplicationContext() {
        return (PotlatchApplication) super.getApplicationContext();
    }

    private boolean isConnected() {
        return mConnectionRepository.findPrimaryConnection(Potlatch.class) != null;
    }

    private void disconnect() {
        mConnectionRepository.removeConnections(mConnectionFactory.getProviderId());
    }

}
