package com.zsoft.android.potlatch.ui.activity;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.zsoft.android.potlatch.Constants;
import com.zsoft.android.potlatch.DaggerInjectionUtils;
import com.zsoft.android.potlatch.R;
import com.zsoft.android.potlatch.model.Gift;
import com.zsoft.android.potlatch.model.GiftForm;
import com.zsoft.android.potlatch.rest.api.GiftOperations;
import com.zsoft.android.potlatch.rest.api.Potlatch;
import com.zsoft.android.potlatch.rest.util.CallableTask;
import com.zsoft.android.potlatch.rest.util.TaskCallback;
import com.zsoft.android.potlatch.util.BitmapWorkerTask;

import org.springframework.social.connect.ConnectionRepository;

import java.io.File;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Date;
import java.util.concurrent.Callable;

import javax.inject.Inject;

public class CreateGiftActivity extends AbstractActionBarDialogActivity {
    protected static final String TAG = CreateGiftActivity.class.getSimpleName();

    public static final int REQUEST_SELECT_PICTURE = 1;

    private static final String KEY_IMAGE_PATH = "sourceImagePath";

    @Inject
    ConnectionRepository mConnectionRepository;

    private Potlatch mPotlatch;

    private Uri mImageCaptureUri;
    private String mImagePath;

    private ImageView mImgContent;
    private TextView mTxtTitle;
    private TextView mTxtDescription;

    private Gift mParentGift;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerInjectionUtils.inject(this);
        setContentView(R.layout.activity_create_gift);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationIcon(R.drawable.ic_up);
        }

        mImgContent     = (ImageView) findViewById(R.id.imgContent);
        mTxtTitle       = (TextView) findViewById(R.id.txtTitle);
        mTxtDescription = (TextView) findViewById(R.id.txtDescription);

        mPotlatch = mConnectionRepository.findPrimaryConnection(Potlatch.class).getApi();

        if (savedInstanceState != null) {
            mImagePath = savedInstanceState.getString(KEY_IMAGE_PATH);
            if (mImagePath != null) {
                new BitmapWorkerTask(mImgContent).execute(mImagePath);
            }
        }

        Serializable serObj = getIntent().getSerializableExtra(Constants.EXTRA_GIFT);
        if (serObj instanceof Gift) {
            mParentGift = (Gift) serObj;
        }

        mImgContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPicture();
            }
        });
    }

    private void addPicture() {
        Intent pickIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // generate file name
        String imageFilePath = MessageFormat.format("{0}/potlatch_{1}.jpg",
                Environment.getExternalStorageDirectory().getAbsolutePath(),
                DateFormat.format("yyyyMMddhhmmss", new Date()));

        File imageFile = new File(imageFilePath);
        mImageCaptureUri = Uri.fromFile(imageFile);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
        takePictureIntent.putExtra("return-data", true);

        String pickTitle = getString(R.string.select_picture);
        Intent chooserIntent = Intent.createChooser(pickIntent, pickTitle);
        chooserIntent.putExtra(
                Intent.EXTRA_INITIAL_INTENTS,
                new Intent[] { takePictureIntent });

        startActivityForResult(chooserIntent, REQUEST_SELECT_PICTURE);
    }

    protected void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString(KEY_IMAGE_PATH, mImagePath);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_SELECT_PICTURE:
                if (resultCode == RESULT_OK) {
                    try {
                        if (data != null) {
                            mImageCaptureUri = data.getData();
                            mImagePath = getRealPathFromURI(mImageCaptureUri);
                        } else {
                            mImagePath = mImageCaptureUri.getPath();
                        }

                        if (mImagePath == null) {
                            return;
                        }

                        new BitmapWorkerTask(mImgContent).execute(mImagePath);
                    } catch (Exception exc) {
                        Log.e(TAG, "Error retrieving the picture.", exc);
                    }
                }
                break;
        }
    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] filePathColumn = { MediaStore.Images.Media.DATA };

        Cursor cursor = null;
        try {
            cursor = getContentResolver().query(contentUri, filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            return cursor.getString(columnIndex);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_gift, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_gift_create:
                saveGift();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveGift() {
        final String title = mTxtTitle.getText().toString();
        final String text  = mTxtDescription.getText().toString();

        if (title == null || title.isEmpty()) {
            Toast.makeText(this, getString(R.string.error_gift_blank_title), Toast.LENGTH_SHORT).show();
            return;
        }

        GiftForm request = new GiftForm(null, title, text);
        if (mParentGift != null && mParentGift.getId() != null) {
            request.setParentId(mParentGift.getId());
        }

        if (mImagePath == null || !new File(mImagePath).exists()) {
            Toast.makeText(this, getString(R.string.error_gift_no_picture), Toast.LENGTH_SHORT).show();
            return;
        }

        saveGift(request, mImagePath);
    }

    private void saveGift(final GiftForm request, final String imagePath) {
        hideSoftKeyboard();
        showProgressDialog(getString(R.string.create_gift_dialog));
        CallableTask.invoke(
                new Callable<Gift>() {
                    @Override
                    public Gift call() throws Exception {
                        GiftOperations operations = mPotlatch.giftOperations();
                        return operations.addGift(request, imagePath);
                    }
                },
                new TaskCallback<Gift>() {
                    @Override
                    public void success(final Gift gift) {
                        dismissProgressDialog();
                        if (gift != null) {
                            Intent intent = new Intent();
                            intent.putExtra(Constants.EXTRA_GIFT, gift);
                            setResult(RESULT_OK);
                            finish();
                        } else {
                            showMessage(getString(R.string.error_create_gift),
                                    getString(R.string.error_unknown));
                        }
                    }

                    @Override
                    public void error(Exception e) {
                        dismissProgressDialog();
                        showMessage(getString(R.string.error_create_gift),
                                e.getLocalizedMessage());
                    }
                }
        );
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}
