package com.zsoft.android.potlatch.rest.connect;

import com.zsoft.android.potlatch.rest.RestTemplateUtils;
import com.zsoft.android.potlatch.rest.util.LoggingRequestInterceptor;
import com.zsoft.android.potlatch.rest.util.PreemptiveBasicAuthClientHttpRequestInterceptor;

import org.springframework.social.oauth2.GrantType;
import org.springframework.social.oauth2.OAuth2Parameters;
import org.springframework.social.oauth2.OAuth2Template;
import org.springframework.web.client.RestTemplate;

public class PotlatchOAuth2Template extends OAuth2Template {
    private final String mClientId;
    private final String mClientSecret;

    public PotlatchOAuth2Template(
            final String baseUrl, final String clientId, final String clientSecret) {
        super(clientId, clientSecret,
                baseUrl + "/oauth/authorize",
                baseUrl + "/oauth/token");

        this.mClientId = clientId;
        this.mClientSecret = clientSecret;
        setUseParametersForClientAuthentication(true);
    }

    @Override
    public String buildAuthenticateUrl(GrantType grantType, OAuth2Parameters parameters) {
        parameters.add("grant_type", "password");

        return super.buildAuthenticateUrl(grantType, parameters);
    }

    @Override
    protected RestTemplate createRestTemplate() {
        return RestTemplateUtils.createUnsafeTemplate(
                new LoggingRequestInterceptor(),
                new PreemptiveBasicAuthClientHttpRequestInterceptor(mClientId, mClientSecret)
        );
    }
}
