package com.zsoft.android.potlatch.util;

public interface Order {
    String getOrderBy();

    boolean isAscendingOrder();
}
