package com.zsoft.android.potlatch.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.zsoft.android.potlatch.AppPreferences;
import com.zsoft.android.potlatch.Constants;
import com.zsoft.android.potlatch.DaggerInjectionUtils;
import com.zsoft.android.potlatch.R;
import com.zsoft.android.potlatch.model.User;
import com.zsoft.android.potlatch.rest.api.Potlatch;
import com.zsoft.android.potlatch.rest.api.UserOperations;
import com.zsoft.android.potlatch.rest.util.CallableTask;
import com.zsoft.android.potlatch.rest.util.TaskCallback;
import com.zsoft.android.potlatch.ui.activity.UserGiftListActivity;
import com.zsoft.android.potlatch.ui.adapter.SimpleEndlessAdapter;
import com.zsoft.android.potlatch.ui.adapter.UserListAdapter;
import com.zsoft.android.potlatch.util.UserOrder;

import org.springframework.social.connect.ConnectionRepository;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import javax.inject.Inject;

public class UserListFragment extends SwipeRefreshListFragment
        implements SwipeRefreshLayout.OnRefreshListener {
    protected static final String TAG = UserListFragment.class.getSimpleName();

    private int pageSize = 10;

    @Inject
    ConnectionRepository mConnectionRepository;

    protected Potlatch mPotlatch;

    private UserListAdapter mAdapter;
    private SimpleEndlessAdapter mEndlessAdapter;

    private ArrayList<User> mUsers;
    private String mSearchQuery = null;

    private boolean mDestroyed = false;

    private int mOrder = UserOrder.ORDER_TOP_GIVERS;

    public UserListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerInjectionUtils.inject(this);

        mPotlatch = mConnectionRepository.findPrimaryConnection(Potlatch.class).getApi();

        pageSize = AppPreferences.getItemsPerPage(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View top = super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);
        return top;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAdapter = new UserListAdapter(getActivity());
        mEndlessAdapter = new SimpleEndlessAdapter(getActivity(), mAdapter) {
            public void doOnLoading() {
                getNextListPage();
            }
        };
        setListAdapter(mEndlessAdapter);
        setEmptyText(getString(R.string.list_no_givers));
        getListView().setDivider(null);

        int height = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 5,
                getActivity().getResources().getDisplayMetrics()
        );
        getListView().setDividerHeight(height);

        if (mUsers != null) {
            setNewList(mUsers, false);
        } else {
            getNewList();
        }

        setListeners();
    }

    protected ArrayList<User> loadPage(String query, int pageId, int pageSize, int order) {
        UserOperations userOperations = mPotlatch.userOperations();
        UserOrder ordering = UserOrder.getOrder(order);
        return new ArrayList<User>(userOperations.searchUsers(query, pageId, pageSize,
                ordering.getOrderBy(), ordering.isAscendingOrder()));
    }

    protected ArrayList<User> loadPage(int pageId, int pageSize, int order) {
        UserOperations userOperations = mPotlatch.userOperations();
        UserOrder ordering = UserOrder.getOrder(order);
        return new ArrayList<User>(userOperations.getAllUsers(pageId, pageSize,
                ordering.getOrderBy(), ordering.isAscendingOrder()));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        inflater.inflate(R.menu.menu_users_list, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = new SearchView(((ActionBarActivity) getActivity())
                .getSupportActionBar().getThemedContext());
        MenuItemCompat.setShowAsAction(item,
                MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);

        MenuItemCompat.setActionView(item, searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Called when the action bar search text has changed. Update
                // the search filter, and reload gifts.
                mSearchQuery = query;
                getNewList();

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // Don't care about this.
                return true;
            }
        });

        MenuItemCompat.setOnActionExpandListener(item, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                mSearchQuery = null;
                getNewList();

                return true;
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch(id) {
            case R.id.action_refresh:
                getNewList();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setListeners() {
        setOnRefreshListener(this);

        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), UserGiftListActivity.class);
                intent.putExtra(Constants.EXTRA_USER, (User) adapterView.getItemAtPosition(position));
                startActivity(intent);
            }
        });
    }

    @Override
    public void onRefresh() {
        getNewList();
    }

    protected void getNewList() {
        getNewList(false);
    }

    private void getNextListPage() {
        getNewList(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mAdapter = null;
        mEndlessAdapter = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mUsers = null;
        mDestroyed = true;
    }

    protected void setNewList(ArrayList<User> newList, boolean appendResults) {
        if (appendResults) {
            if (newList == null) {
                this.mUsers = null;
            } else {
                this.mUsers.addAll(newList);
            }
        } else {
            this.mUsers = newList;
        }

        if (getView() == null) {
            return;
        }

        setRefreshing(false);
        setListShown(true);

        mAdapter.setItems(mUsers);
        mEndlessAdapter.onDataReady();
        if (newList != null && newList.size() >= pageSize) {
            mEndlessAdapter.restartAppending();
        } else {
            mEndlessAdapter.stopAppending();
        }

        if (!appendResults && !mEndlessAdapter.isEmpty()) {
            getListView().setSelection(0);	// reset list scroll position after setting new list
        }
    }

    private void getNewList(final boolean isNextPage) {
        if (!isNextPage) {
            mUsers = null;
        }

        try {
            final int pageId = (mUsers != null ? mUsers.size() / pageSize : 0);

            CallableTask.invoke(
                    new Callable<ArrayList<User>>() {
                        @Override
                        public ArrayList<User> call() throws Exception {
                            if (!TextUtils.isEmpty(mSearchQuery)) {
                                return loadPage(mSearchQuery, pageId, pageSize, mOrder);
                            }
                            return loadPage(pageId, pageSize, mOrder);
                        }
                    },
                    new TaskCallback<ArrayList<User>>() {
                        @Override
                        public void success(ArrayList<User> gifts) {
                            if (getActivity() != null && !mDestroyed) {
                                setNewList(gifts, isNextPage);
                            }
                        }

                        @Override
                        public void error(Exception e) {
                            if (getActivity() != null && !mDestroyed) {
                                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
                                setNewList(null, isNextPage);
                            }
                        }
                    });

            if (!isNextPage && getListView() != null) {
                // hide ListView and show progress bar only when loading the first page
                setListShownNoAnimation(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
            setNewList(null, isNextPage);
        }
    }

    public void setOrder(int order) {
        mOrder = order;
    }

    public void reloadUsers() {
        getNewList();
    }

}
