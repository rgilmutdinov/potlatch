package com.zsoft.android.potlatch.rest.api.impl;

import com.zsoft.android.potlatch.model.User;
import com.zsoft.android.potlatch.rest.api.UserOperations;

import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

public class UserTemplate extends AbstractPotlatchOperations implements UserOperations {

    public UserTemplate(final String baseUrl, final String accessToken) {
        super(baseUrl, accessToken);
    }

    @Override
    public User getProfile() {
        return getRestTemplate().getForObject(buildUri("/users/me"), User.class);
    }

    @Override
    public List<User> searchUsers(String query, int pageId, int pageSize, String orderBy, boolean ascendingOrder) {
        ResponseEntity<User[]> entity =
                getRestTemplate().getForEntity(buildUri("/users/search?q={q}&p={p}&l={l}&s={s}&d={d}"),
                        User[].class, query, pageId, pageSize, orderBy, ascendingOrder ? "asc" : "desc");

        return Arrays.asList(entity.getBody());
    }

    @Override
    public List<User> getAllUsers(int pageId, int pageSize, String orderBy, boolean ascendingOrder) {
        ResponseEntity<User[]> entity =
                getRestTemplate().getForEntity(buildUri("/users?p={p}&l={l}&s={s}&d={d}"),
                        User[].class, pageId, pageSize, orderBy, ascendingOrder ? "asc" : "desc");

        return Arrays.asList(entity.getBody());
    }
}
