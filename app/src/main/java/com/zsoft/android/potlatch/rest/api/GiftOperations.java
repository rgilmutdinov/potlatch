package com.zsoft.android.potlatch.rest.api;

import com.zsoft.android.potlatch.model.Gift;
import com.zsoft.android.potlatch.model.GiftForm;

import java.util.List;

public interface GiftOperations {
    List<Gift> getGifts(boolean includeNegativeRated, int page, int pageSize);
    List<Gift> getGifts(boolean includeNegativeRated, int page, int pageSize, String orderBy, boolean ascendingOrder);
    List<Gift> getMyGifts(int page, int pageSize, String orderBy, boolean ascendingOrder);
    List<Gift> getGiftsOfUser(long ownerId, boolean includeNegativeRated, int page, int pageSize, String orderBy, boolean ascendingOrder);

    List<Gift> getChainedGifts(long parentId, boolean includeNegativeRated, int page, int pageSize);
    List<Gift> getChainedGifts(long parentId, boolean includeNegativeRated, int page, int pageSize, String orderBy, boolean ascendingOrder);

    List<Gift> searchGifts(String query, boolean includeNegativeRated, int page, int pageSize);
    List<Gift> searchGifts(String query, boolean includeNegativeRated, int page, int pageSize, String orderBy, boolean ascendingOrder);
    List<Gift> searchMyGifts(String query, int page, int pageSize, String orderBy, boolean ascendingOrder);
    List<Gift> searchGiftsOfUser(long ownerId, String query, boolean includeNegativeRated, int page, int pageSize, String orderBy, boolean ascendingOrder);

    List<Gift> searchChainedGifts(long parentId, String query, boolean includeNegativeRated, int page, int pageSize);
    List<Gift> searchChainedGifts(long parentId, String query, boolean includeNegativeRated, int page, int pageSize, String orderBy, boolean ascendingOrder);

    List<Gift> refreshGifts(List<Long> ids);

    Gift getGift(long id);
    boolean deleteGift(long id);
    Gift addGift(GiftForm giftForm, String imagePath);

    Gift giftVoteUp(long id);
    Gift giftVoteDown(long id);
}
