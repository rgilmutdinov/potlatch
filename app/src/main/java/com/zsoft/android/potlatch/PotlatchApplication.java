package com.zsoft.android.potlatch;

import android.app.Application;

import com.zsoft.android.potlatch.service.AlarmReceiver;

import java.util.Arrays;
import java.util.List;

import dagger.ObjectGraph;

public class PotlatchApplication extends Application {
    private ObjectGraph objectGraph;

    @Override
    public void onCreate() {
        super.onCreate();
        Object[] modules = getModules().toArray();
        objectGraph = ObjectGraph.create(modules);

        if (AppPreferences.autoRefreshEnabled(this)) {
            AlarmReceiver.start(this);
        }
    }

    protected List<Object> getModules() {
        return Arrays.<Object>asList(new PotlatchModule(this));
    }

    public void inject(Object potlatchComponent) {
        this.objectGraph.inject(potlatchComponent);
    }
}
