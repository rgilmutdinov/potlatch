package com.zsoft.android.potlatch.ui.fragment;

import android.accounts.Account;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zsoft.android.potlatch.AppPreferences;
import com.zsoft.android.potlatch.Constants;
import com.zsoft.android.potlatch.R;
import com.zsoft.android.potlatch.model.Gift;
import com.zsoft.android.potlatch.model.User;
import com.zsoft.android.potlatch.rest.api.GiftOperations;
import com.zsoft.android.potlatch.ui.activity.ChainedGiftListActivity;
import com.zsoft.android.potlatch.ui.adapter.AbstractGiftListAdapter;
import com.zsoft.android.potlatch.ui.adapter.GiftListAdapter;
import com.zsoft.android.potlatch.util.AccountUtils;
import com.zsoft.android.potlatch.util.GiftOrder;

import java.text.MessageFormat;
import java.util.ArrayList;

public class UserGiftListFragment extends PaginatedGiftListFragment implements View.OnClickListener {
    protected static final String TAG = UserGiftListFragment.class.getSimpleName();

    private User mUser;

    public UserGiftListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View top = super.onCreateView(inflater, container, savedInstanceState);

        mFloatingActionButton.setVisibility(View.GONE);
        Bundle bundle = getActivity().getIntent().getExtras();
        mUser = (User) bundle.getSerializable(Constants.EXTRA_USER);

        if (mUser != null) {
            String username = mUser.getUsername();
            if (!TextUtils.isEmpty(username)) {
                getActivity().setTitle(MessageFormat.format(
                        getActivity().getString(R.string.gifts_of_user_title), username));
            }
        }

        return top;
    }

    @Override
    public void onClick(View v) {
        Object tag = v.getTag();
        if (tag != null) {
            final int position = Integer.parseInt(tag.toString());
            Gift gift = getAdapter().getItem(position);
            switch (v.getId()) {
                case R.id.gift_chain:
                    Intent intent = new Intent(getActivity(), ChainedGiftListActivity.class);
                    intent.putExtra(Constants.EXTRA_GIFT, gift);
                    startActivity(intent);
                    break;
                case R.id.gift_delete:
                    mPotlatch.giftOperations().deleteGift(gift.getId());
                    break;
            }
        }
    }

    @Override
    protected void configureGiftCreateIntent(final Intent intent) {
        intent.putExtra(Constants.EXTRA_USER, mUser);
    }

    protected AbstractGiftListAdapter createAdapter() {
        Account chosenAccount = AccountUtils.getActiveAccount(getActivity());
        Long userId = null;
        if (chosenAccount != null) {
            userId = AccountUtils.getProfileId(getActivity());
        }
        return new GiftListAdapter(getActivity(), this, userId);
    }

    @Override
    protected ArrayList<Gift> loadPage(String query, int pageId, int pageSize, int order) {
        if (mUser == null || mUser.getId() == null) {
            return null;
        }

        boolean includeNegativeRated = !AppPreferences.hideObsceneGifts(getActivity());

        GiftOrder ordering = GiftOrder.getOrder(order);
        GiftOperations giftOperations = mPotlatch.giftOperations();
        return new ArrayList<Gift>(giftOperations.searchGiftsOfUser(
                mUser.getId(), query, includeNegativeRated, pageId, pageSize,
                ordering.getOrderBy(), ordering.isAscendingOrder()));
    }

    protected ArrayList<Gift> loadPage(int pageId, int pageSize, int order) {
        if (mUser == null || mUser.getId() == null) {
            return null;
        }

        boolean includeNegativeRated = !AppPreferences.hideObsceneGifts(getActivity());

        GiftOrder ordering = GiftOrder.getOrder(order);
        GiftOperations giftOperations = mPotlatch.giftOperations();
        return new ArrayList<Gift>(giftOperations.getGiftsOfUser(
                mUser.getId(), includeNegativeRated, pageId, pageSize,
                ordering.getOrderBy(), ordering.isAscendingOrder()));
    }
}