package com.zsoft.android.potlatch;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.support.v4.app.Fragment;

public class DaggerInjectionUtils {

    public static void inject(Fragment fragment) {
        PotlatchApplication potlatch = forApplication(fragment.getActivity().getApplication());
        potlatch.inject(fragment);
    }

    public static void inject(Activity baseActivity) {
        PotlatchApplication potlatch = forApplication(baseActivity.getApplication());
        potlatch.inject(baseActivity);
    }

    public static void inject(Service service) {
        PotlatchApplication potlatch = forApplication(service.getApplication());
        potlatch.inject(service);
    }

    private static PotlatchApplication forApplication(Application application) {
        if (application instanceof PotlatchApplication){
            return (PotlatchApplication) application;
        }
        return null;
    }
}