package com.zsoft.android.potlatch.util;

import android.accounts.Account;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

public class AccountUtils {

    private static final String TAG = AccountUtils.class.getSimpleName();

    private static final String POTLATCH_ACCOUNT_TYPE   = "com.zsoft.potlatch";
    private static final String PREF_ACTIVE_ACCOUNT     = "active_account";
    private static final String PREFIX_PREF_PROFILE_ID  = "profile_id_";
    private static final String PREFIX_PREF_NAME        = "name_";
    private static final String PREFIX_PREF_IMAGE_URL   = "image_url_";
    private static final String PREFIX_PREF_EMAIL       = "email_";

    private static SharedPreferences getSharedPreferences(final Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static String getActiveAccountName(final Context context) {
        SharedPreferences sp = getSharedPreferences(context);
        return sp.getString(PREF_ACTIVE_ACCOUNT, null);
    }

    public static Account getActiveAccount(final Context context) {
        String account = getActiveAccountName(context);
        if (account != null) {
            return new Account(account, POTLATCH_ACCOUNT_TYPE);
        } else {
            return null;
        }
    }

    public static boolean setActiveAccount(final Context context, final String accountName) {
        Log.d(TAG, "Set active account to: " + accountName);
        SharedPreferences sp = getSharedPreferences(context);
        sp.edit().putString(PREF_ACTIVE_ACCOUNT, accountName).apply();
        return true;
    }

    public static String getName(final Context context) {
        SharedPreferences sp = getSharedPreferences(context);
        return hasActiveAccount(context) ? sp.getString(makeAccountSpecificPrefKey(context,
                PREFIX_PREF_NAME), null) : null;
    }

    private static String makeAccountSpecificPrefKey(Context ctx, String prefix) {
        return hasActiveAccount(ctx) ? makeAccountSpecificPrefKey(getActiveAccountName(ctx),
                prefix) : null;
    }

    private static String makeAccountSpecificPrefKey(String accountName, String prefix) {
        return prefix + accountName;
    }

    public static boolean hasActiveAccount(final Context context) {
        return !TextUtils.isEmpty(getActiveAccountName(context));
    }

    public static String getImageUrl(final Context context) {
        SharedPreferences sp = getSharedPreferences(context);
        return hasActiveAccount(context) ? sp.getString(makeAccountSpecificPrefKey(context,
                PREFIX_PREF_IMAGE_URL), null) : null;
    }

    public static String getEmail(final Context context) {
        SharedPreferences sp = getSharedPreferences(context);
        return hasActiveAccount(context) ? sp.getString(makeAccountSpecificPrefKey(context,
                PREFIX_PREF_EMAIL), null) : null;
    }

    public static Long getProfileId(final Context context) {
        SharedPreferences sp = getSharedPreferences(context);
        return hasActiveAccount(context) ? sp.getLong(makeAccountSpecificPrefKey(context,
                PREFIX_PREF_PROFILE_ID), -1) : null;
    }

    public static void setProfileId(final Context context, final String accountName, final Long profileId) {
        SharedPreferences sp = getSharedPreferences(context);
        sp.edit().putLong(makeAccountSpecificPrefKey(accountName, PREFIX_PREF_PROFILE_ID),
                profileId).apply();
    }

    public static void setImageUrl(final Context context, final String accountName, final String imageUrl) {
        SharedPreferences sp = getSharedPreferences(context);
        sp.edit().putString(makeAccountSpecificPrefKey(accountName, PREFIX_PREF_IMAGE_URL),
                imageUrl).apply();
    }

    public static void setName(final Context context, final String accountName, final String name) {
        SharedPreferences sp = getSharedPreferences(context);
        sp.edit().putString(makeAccountSpecificPrefKey(accountName, PREFIX_PREF_NAME),
                name).apply();
    }

    public static void setEmail(final Context context, final String accountName, String coverPhotoUrl) {
        SharedPreferences sp = getSharedPreferences(context);
        sp.edit().putString(makeAccountSpecificPrefKey(accountName, PREFIX_PREF_EMAIL),
                coverPhotoUrl).apply();
    }
}
