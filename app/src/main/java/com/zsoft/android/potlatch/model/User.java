package com.zsoft.android.potlatch.model;

import com.google.common.base.Objects;

public class User extends Entity {
    private String username;
    private String email;
    private Long rating;
    private Long giftsPosted;
    private String gravatarUrl;

    public User() {
        
    }

    public User(Long id, String username, String email) {
        this.id = id;
        this.username = username;
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getRating() {
        return rating;
    }

    public void setRating(Long rating) {
        this.rating = rating;
    }

    public Long getGiftsPosted() {
        return giftsPosted;
    }

    public void setGiftsPosted(Long giftsPosted) {
        this.giftsPosted = giftsPosted;
    }

    public String getGravatarUrl() {
        return gravatarUrl;
    }

    public void setGravatarUrl(String gravatarUrl) {
        this.gravatarUrl = gravatarUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof User) {
            User other = (User) o;
            return Objects.equal(id, other.id) &&
                   Objects.equal(username, other.username) &&
                   Objects.equal(email, other.email);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, username, email);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("id", id)
                .add("username", username)
                .add("email", email)
                .toString();
    }
}
