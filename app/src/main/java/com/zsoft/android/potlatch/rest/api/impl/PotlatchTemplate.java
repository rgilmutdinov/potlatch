package com.zsoft.android.potlatch.rest.api.impl;

import com.zsoft.android.potlatch.rest.api.GiftOperations;
import com.zsoft.android.potlatch.rest.api.Potlatch;
import com.zsoft.android.potlatch.rest.api.UserOperations;

import org.springframework.web.client.RestTemplate;

public class PotlatchTemplate extends AbstractPotlatchOperations implements Potlatch {

    private UserOperations mUserOperations;
    private GiftOperations mGiftOperations;

    public PotlatchTemplate(final String baseUrl, final String accessToken) {
        super(baseUrl, accessToken);

        mUserOperations = new UserTemplate(baseUrl, accessToken);
        mGiftOperations = new GiftTemplate(baseUrl, accessToken);
    }

    @Override
    public UserOperations userOperations() {
        return mUserOperations;
    }

    @Override
    public RestTemplate restOperations() {
        return getRestTemplate();
    }

    @Override
    public GiftOperations giftOperations() {
        return mGiftOperations;
    }
}
